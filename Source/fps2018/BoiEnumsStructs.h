// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/Texture2D.h"

#include "BoiEnumsStructs.generated.h"

/* Don't forget to add these game modes to the DefaultGame.ini file when adding new ones */
UENUM(BlueprintType)
enum class EGameMode : uint8
{
	FFA 	UMETA(DisplayName = "FreeForAll"),
	TDM 	UMETA(DisplayName = "TeamDeathMatch")
};

USTRUCT(BlueprintType)
struct FMapInfo
{
	GENERATED_BODY();

	UPROPERTY(EditDefaultsOnly, Category = "MapInfo")
	FString InGameName = "New Map";

	UPROPERTY(EditDefaultsOnly, Category = "MapInfo")
	UTexture2D* MapIcon = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "MapInfo")
	FString EditorName = "";
};

USTRUCT(BlueprintType)
struct FPlayerInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FText Name = FText::FromString("Bobby");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UTexture2D* PlayerImage;

	// TODO: Weapons should be in a separate struct, this need to replicate with server/owner only while the other props in here need to replicate to everyone.
	// Or how would other players know what a player's weapon is if this isn't replicated?
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class ABoiWeapon> PrimaryWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class ABoiWeapon> SecondaryWeapon;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	uint8 Team;

	// TODO: Make this an enum or a bool
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FText PlayerStatus = FText::FromString("Not Ready!");

	FPlayerInfo()
	{
		FString Path = "/Engine/EngineResources/AICON-Red";
		PlayerImage = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, *(Path)));
	}

};

USTRUCT(BlueprintType)
struct FGameSettings
{
	GENERATED_BODY()

public:

	UPROPERTY()
	bool bInitialized;

	/** Server name chosen by host user */
	UPROPERTY()
	FString ServerName;

	/** Number of players chosen by host user */
	UPROPERTY()
	int32 MaxPlayerCount;

	/** In seconds, how long a match will last */
	UPROPERTY()
	uint32 TimeLimit;

	/** How many point are required for a win */
	UPROPERTY()
	uint32 ScoreLimit;

	UPROPERTY()
	EGameMode GameMode;

	UPROPERTY()
	int MapID;
};