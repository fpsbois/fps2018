// Copyright FPSBois 2018

#include "BoiCreateAClassCard.h"

#include "BoiGameInstance.h"
#include "BoiCreateAClassMenu.h"
#include "BoiCreateAClassSave.h"
#include "BoiWeaponLibrary.h"
#include "BoiWeaponDataAsset.h"


void UBoiCreateAClassCard::Setup(UBoiCreateAClassMenu* NewCreateAClassMenu, UBoiCreateAClassSave* NewSaveSlot)
{
	CreateAClassMenu = NewCreateAClassMenu;
	SaveSlot = NewSaveSlot;

	UpdatePrimaryWeapon(SaveSlot->PrimaryWeaponIndex);
	UpdateSecondaryWeapon(SaveSlot->SecondaryWeaponIndex);
}

void UBoiCreateAClassCard::UpdatePrimaryWeapon(int WeaponIndex)
{
	if (WeaponIndex < 0) 
	{
		UE_LOG(LogTemp, Warning, TEXT("Primary Weapon Index %d cannot be < 0."), WeaponIndex);
		WeaponIndex = 0; 
	}

	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetWorld()->GetGameInstance()); 

	if (WeaponIndex >= GI->GetWeaponLibrary()->PrimaryWeapons.Num()) 
	{
		UE_LOG(LogTemp, Warning, TEXT("Primary Weapon Index %d cannot be > number of primary weapons."), WeaponIndex);
		WeaponIndex = 0; 
	}

	PrimaryWeaponIndex = WeaponIndex;
	SaveSlot->PrimaryWeaponIndex = WeaponIndex;
	CurrentPrimaryWeapon = GI->GetWeaponLibrary()->PrimaryWeapons[WeaponIndex];
	UpdatePrimaryGraphics();
	CreateAClassMenu->SaveAllClasses();
}

void UBoiCreateAClassCard::UpdateSecondaryWeapon(int WeaponIndex)
{
	if (WeaponIndex < 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Secondary Weapon Index %d cannot be < 0."), WeaponIndex);
		WeaponIndex = 0;
	}
	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetWorld()->GetGameInstance());
	if (WeaponIndex >= GI->GetWeaponLibrary()->SecondaryWeapons.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("Secondary Weapon Index %d cannot be > number of secondary weapons."), WeaponIndex);
		WeaponIndex = 0;
	}

	SaveSlot->SecondaryWeaponIndex = WeaponIndex;
	SecondaryWeaponIndex = WeaponIndex;
	CurrentSecondaryWeapon = GI->GetWeaponLibrary()->SecondaryWeapons[WeaponIndex];
	UpdateSecondaryGraphics();
	CreateAClassMenu->SaveAllClasses();
}


