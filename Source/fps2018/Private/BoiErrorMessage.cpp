// Copyright © FPSBois 2018

#include "BoiErrorMessage.h"

#include "BoiGameInstance.h"

void UBoiErrorMessage::ConfirmMessage()
{
	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetOwningPlayer()->GetGameInstance());
	if (GI)
	{
		GI->DestroySession(GetOwningPlayer());
		GI->QuitToMainMenu();
	}

	RemoveFromParent();
}


