// Copyright © FPSBois 2018

#include "BoiHealthComponent.h"

#include "Blueprint/UserWidget.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

#include "BoiGameMode.h"

UBoiHealthComponent::UBoiHealthComponent()
{
	TeamNum = 255;
	MaxHealth = 100;
	HealthRegenTick = .25f;
	HealthRegenAmount = 30.0f;
	NonCombatTime = 3.0f;

	SetIsReplicated(true);
}

void UBoiHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	AActor* Owner = GetOwner();

	if (Owner)
	{
		if (GetOwnerRole() == ROLE_Authority)
		{
			Owner->OnTakeAnyDamage.AddDynamic(this, &UBoiHealthComponent::OnTakeAnyDamage);
		}
	}

	CurrentHealth = MaxHealth;
}

void UBoiHealthComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void UBoiHealthComponent::OnHealingTick()
{
	Heal(HealthRegenAmount* HealthRegenTick);
}

void UBoiHealthComponent::StartHealing()
{
	if (GetOwner())
	{
		GetOwner()->GetWorldTimerManager().SetTimer(TimerHandle_Healing, this, &UBoiHealthComponent::OnHealingTick, HealthRegenTick, true);
	}
}

void UBoiHealthComponent::Multicast_Die_Implementation(const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (GetOwner()->HasAuthority())
	{
		GetOwner()->GetWorldTimerManager().ClearAllTimersForObject(GetOwner());
	}
	
	OnHealthDead.Broadcast(DamageType, InstigatedBy, DamageCauser);
}

// Only called on client
void UBoiHealthComponent::OnRep_Health(float OldHealth)
{
	float Damage = CurrentHealth - OldHealth;
	OnHealthChanged.Broadcast(this, CurrentHealth, Damage, nullptr, nullptr, nullptr);
}

// Only called on server, called when owner takes damage on server see hook in begin play
void UBoiHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	AActor* Owner = GetOwner();
	if (!Owner) { return; }

	if (Damage <= 0.0f || IsDead())
	{
		return;
	}

	/*if (DamageCauser != DamagedActor && IsFriendly(DamagedActor, DamageCauser))
	{
		return;
	}*/

	CurrentHealth = FMath::Clamp(CurrentHealth - Damage, 0.0f, MaxHealth);
	OnHealthChanged.Broadcast(this, CurrentHealth, Damage, DamageType, InstigatedBy, DamageCauser);

	if (IsDead())
	{
		Multicast_Die(DamageType, InstigatedBy, DamageCauser);
	}
	else 
	{
		Owner->GetWorldTimerManager().ClearTimer(TimerHandle_Healing);
		Owner->GetWorldTimerManager().SetTimer(TimerHandle_NonCombat, this, &UBoiHealthComponent::StartHealing, NonCombatTime, false);
	}

	UE_LOG(LogTemp, Log, TEXT("Health Changed: %s (-%s)"), *FString::SanitizeFloat(CurrentHealth), *FString::SanitizeFloat(Damage));
}

void UBoiHealthComponent::Heal(float HealAmount)
{
	if (HealAmount <= 0.0f || IsDead())
	{
		return;
	}

	CurrentHealth = FMath::Clamp(CurrentHealth + HealAmount, CurrentHealth, MaxHealth);
	OnHealthChanged.Broadcast(this, CurrentHealth, -HealAmount, nullptr, nullptr, nullptr);
}

bool UBoiHealthComponent::IsFriendly(AActor* ActorA, AActor* ActorB)
{
	if (!ActorA || !ActorB)
	{
		return false;
	}

	UBoiHealthComponent* HealthCmopA = Cast<UBoiHealthComponent>(ActorA->GetComponentByClass(UBoiHealthComponent::StaticClass()));
	UBoiHealthComponent* HealthCmopB = Cast<UBoiHealthComponent>(ActorB->GetComponentByClass(UBoiHealthComponent::StaticClass()));

	if (!HealthCmopA || !HealthCmopB)
	{
		return false;
	}

	return HealthCmopA->TeamNum == HealthCmopB->TeamNum;
}

void UBoiHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBoiHealthComponent, CurrentHealth);
	DOREPLIFETIME(UBoiHealthComponent, TeamNum);
}