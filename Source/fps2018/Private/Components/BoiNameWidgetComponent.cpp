// Copyright © FPSBois 2018

#include "BoiNameWidgetComponent.h"

#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

#include "BoiHealthComponent.h"
#include "BoiNameWidget.h"

UBoiNameWidgetComponent::UBoiNameWidgetComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.SetTickFunctionEnable(true);

	bReplicates = true;
}

void UBoiNameWidgetComponent::BeginPlay()
{
	Super::BeginPlay();

	UBoiHealthComponent* HealthComp = Cast<UBoiHealthComponent>(GetOwner()->GetComponentByClass(UBoiHealthComponent::StaticClass()));
	if (HealthComp)
	{
		HealthComp->OnHealthDead.AddDynamic(this, &UBoiNameWidgetComponent::OnOwnerDied);
	}
}

void UBoiNameWidgetComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bActive && Target && GetOwner() && Target != GetOwner())
	{
		/*FRotator PlayerRot = (Target->GetActorLocation() - GetOwner()->GetActorLocation()).Rotation();
		SetWorldRotation(PlayerRot);*/
	}
}

void UBoiNameWidgetComponent::ActivateNameComponent(bool bTeammate)
{
	if(GetOwner())
	{
		UBoiHealthComponent* HealthComp = Cast<UBoiHealthComponent>(GetOwner()->GetComponentByClass(UBoiHealthComponent::StaticClass()));
		if (HealthComp && HealthComp->IsDead())
		{
			return;
		}

		GetOwner()->GetWorldTimerManager().SetTimer(TimerHandle_DeactivationTimer, this, &UBoiNameWidgetComponent::DeactivateNameComponent, DeactivateAfterXSeconds, false);
	}

	if (bActive)
	{
		return;
	}

	bActive = true;

	// Server will have ALL controllers, clients will only have one
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; It++)
	{
		APlayerController* Controller = It->Get();
		// because the server has all PCs, this is to check if it is controlled locally.
		if (Controller && Controller->IsLocalController())
		{
			Target = Controller->GetPawn();
		}
	}

	UBoiNameWidget* NameWidget = Cast<UBoiNameWidget>(GetUserWidgetObject());
	if (NameWidget)
	{
		if (bTeammate)
		{
			NameWidget->ShowTeamName();
		}
		else
		{
			NameWidget->ShowEnemyName();
		}
	}
}

void UBoiNameWidgetComponent::DeactivateNameComponent()
{
	if (GetOwner())
	{
		GetOwner()->GetWorldTimerManager().ClearTimer(TimerHandle_DeactivationTimer);
	}

	bActive = false;

	UBoiNameWidget* NameWidget = Cast<UBoiNameWidget>(GetUserWidgetObject());
	if (NameWidget)
	{
		NameWidget->HideName();
	}

	Target = nullptr;
}

void UBoiNameWidgetComponent::SetName(FString NewName)
{
	Name = NewName;
	OnRep_Name();
}

void UBoiNameWidgetComponent::OnOwnerDied(const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	Widget->SetVisibility(ESlateVisibility::Hidden);
}

void UBoiNameWidgetComponent::OnRep_Name()
{
	UBoiNameWidget* NameWidget = Cast<UBoiNameWidget>(GetUserWidgetObject());
	if (NameWidget)
	{
		NameWidget->SetPlayerNameText(Name);
	}
}

void UBoiNameWidgetComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBoiNameWidgetComponent, Name);
}