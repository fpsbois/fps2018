// Copyright FPSBois 2018

#include "BoiPlayerDetectComponent.h"

#include "EngineUtils.h"
#include "TimerManager.h"
#include "Components/WidgetComponent.h"
#include "Blueprint/UserWidget.h"
#include "DrawDebugHelpers.h"

#include "fps2018.h"
#include "BoiHealthComponent.h"
#include "BoiPlayerController.h"
#include "BoiNameWidgetComponent.h"

static int32 DebugDetectionDrawing = 0;
FAutoConsoleVariableRef CVARDebugDetectionDrawing(
	TEXT("Boi.DebugDetection"),
	DebugDetectionDrawing,
	TEXT("Draw Debug Lines for player detection"),
	ECVF_Cheat
);

// Sets default values for this component's properties
UBoiPlayerDetectComponent::UBoiPlayerDetectComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	LookDetectionDistance = 2500.0f;
	VacinityDetectionDistance = 2500.0f;
}

void UBoiPlayerDetectComponent::Client_ActivateDetection_Implementation()
{
	APawn* MyOwner = Cast<APawn>(GetOwner());
	if (MyOwner && MyOwner->IsLocallyControlled())
	{
		GetOwner()->GetWorldTimerManager().SetTimer(TimerHandle_DetectionTimer, this, &UBoiPlayerDetectComponent::OnDetectionTimerTick, RefreshRate, true);
	}
}

// Called when the game starts
void UBoiPlayerDetectComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UBoiPlayerDetectComponent::OnDetectionTimerTick()
{
	CheckForTeam();
	CheckTowardsFacingDirection();
}

void UBoiPlayerDetectComponent::CheckTowardsFacingDirection()
{
	APawn* MyOwner = Cast<APawn>(GetOwner());

	if (!MyOwner) { return; }

	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(MyOwner);
	QueryParams.bTraceComplex = false;
	QueryParams.bReturnPhysicalMaterial = false;

	FVector EyeLocation;
	FRotator EyeRotation;
	MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

	FVector TraceStart = EyeLocation;
	FVector TraceDestination = TraceStart + MyOwner->GetActorForwardVector() * LookDetectionDistance;

	FHitResult Hit;
	/// Check for a hit
	if (GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceDestination, COLLISION_PLAYER, QueryParams))
	{
		AActor* OtherActor = Hit.GetActor();

		if (OtherActor)
		{
			if (DebugDetectionDrawing > 0)
			{
				DrawDebugSphere(GetWorld(), OtherActor->GetActorLocation(), 50, 10, FColor::Red, false, RefreshRate, 0, 2);
			}

			UBoiNameWidgetComponent* WidgetComp = OtherActor->FindComponentByClass<UBoiNameWidgetComponent>();

			if (WidgetComp && UBoiHealthComponent::IsFriendly(MyOwner, OtherActor))
			{
				WidgetComp->ActivateNameComponent(true);

				auto PC = Cast<ABoiPlayerController>(MyOwner->GetController());
				if (PC)
				{
					PC->SetReticleColor(EDetectionType::Ally);
				}
				
			}
			else if (WidgetComp)
			{
				WidgetComp->ActivateNameComponent(false);

				auto PC = Cast<ABoiPlayerController>(MyOwner->GetController());
				if (PC)
				{
					PC->SetReticleColor(EDetectionType::Enemy);
				}
			}
		}
	}
	else
	{
		auto PC = Cast<ABoiPlayerController>(MyOwner->GetController());
		if (PC)
		{
			PC->SetReticleColor(EDetectionType::Default);
		}
	}

	if (DebugDetectionDrawing > 0)
	{
		DrawDebugLine(GetWorld(), TraceStart, TraceDestination, FColor::Red, false, RefreshRate);
	}
}

void UBoiPlayerDetectComponent::CheckForTeam()
{
	APawn* MyOwner = Cast<APawn>(GetOwner());

	if (!MyOwner) { return; }

	/*FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(MyOwner);
	QueryParams.bTraceComplex = false;
	QueryParams.bReturnPhysicalMaterial = false;

	FVector EyeLocation;
	FRotator EyeRotation;
	MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);
	FVector TraceStart = EyeLocation;

	for (TActorIterator<APawn> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		APawn* OtherPawn = *ActorItr;

		if (!OtherPawn || OtherPawn == MyOwner) { continue; }

		FVector EyeLocation2;
		FRotator EyeRotation2;
		OtherPawn->GetActorEyesViewPoint(EyeLocation2, EyeRotation2);
		FVector TraceDirection = (EyeLocation2 - TraceStart).GetSafeNormal();
		FVector TraceDestination = TraceStart + TraceDirection * TeamDectectionDistance;
		FHitResult Hit;
		/// Check for a hit
		if (GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceDestination, ECC_Visibility, QueryParams))
		{

			if (Hit.GetActor() == OtherPawn && UBoiHealthComponent::IsFriendly(MyOwner, OtherPawn))
			{
				if (DebugDetectionDrawing > 0)
				{
					DrawDebugSphere(GetWorld(), OtherPawn->GetActorLocation(), 50, 10, FColor::Blue, false, RefreshRate, 0, 2);
				}

				UBoiNameWidgetComponent* WidgetComp = OtherPawn->FindComponentByClass<UBoiNameWidgetComponent>();

				if (!WidgetComp) { continue; }

				WidgetComp->ActivateNameComponent(true);
			}
		}

		if (DebugDetectionDrawing > 0)
		{
			DrawDebugLine(GetWorld(), TraceStart, TraceDestination, FColor::Blue, false, RefreshRate);
		}
	}*/

	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(MyOwner);

	FVector EyeLocation;
	FRotator EyeRotation;
	MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);
	FVector TraceStart = EyeLocation;

	FCollisionShape CollisionSphere = FCollisionShape::MakeSphere(VacinityDetectionDistance);
	TArray<FHitResult> OutResults;
	GetWorld()->SweepMultiByChannel(OutResults, TraceStart, TraceStart, FQuat::Identity, COLLISION_PLAYER, CollisionSphere, QueryParams);

	for (FHitResult Hit : OutResults)
	{
		if (UBoiHealthComponent::IsFriendly(MyOwner, Hit.GetActor()))
		{
			UBoiNameWidgetComponent* WidgetComp = Hit.GetActor()->FindComponentByClass<UBoiNameWidgetComponent>();

			if (!WidgetComp) { continue; }

			WidgetComp->ActivateNameComponent(true);
		}
	}
}
