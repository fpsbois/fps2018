// Copyright FPSBois 2018

#include "BoiGameInstance.h"

#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "OnlineSessionSettings.h"
#include "MoviePlayer.h" // This is for the loading screen stuff
#include "UnrealNames.h" // This is for NAME_GameSession, useful for using the NULL Online Subsystem

#include "BoiErrorMessage.h"
#include "BoiMainMenu.h"
#include "MenuWidget.h"

const static FName SERVER_NAME_SETTINGS_KEY = TEXT("FPSBoiServerName");

UBoiGameInstance::UBoiGameInstance(const FObjectInitializer &ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(MAIN_MENU_CLASS_PATH);
	if (!ensure(MenuBPClass.Class != nullptr)) { return; }
	MainMenuClass = MenuBPClass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> InGameMenuBPClass(INGAME_MENU_CLASS_PATH);
	if (!ensure(InGameMenuBPClass.Class != nullptr)) { return; }
	InGameMenuClass = InGameMenuBPClass.Class;

}

void UBoiGameInstance::Init()
{
	Super::Init();

	IOnlineSubsystem* OSS = IOnlineSubsystem::Get();
	if (OSS)
	{
		UE_LOG(LogTemp, Warning, TEXT("Found OSS %s"), *OSS->GetSubsystemName().ToString());
		SessionInterface = OSS->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UBoiGameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UBoiGameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UBoiGameInstance::OnFindSessionsComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UBoiGameInstance::OnJoinSessionComplete);
		}
		if (OSS->GetSubsystemName().ToString() == "Steam")
		{
			bSteam = true;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Found no OSS"));
	}

	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UBoiGameInstance::BeginLoadingScreen);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &UBoiGameInstance::EndLoadingScreen);
	
	/// Error handling
	GetEngine()->OnNetworkFailure().AddUObject(this, &UBoiGameInstance::OnNetworkFailure);
	GetEngine()->OnTravelFailure().AddUObject(this, &UBoiGameInstance::OnTravelFailure);
}

void UBoiGameInstance::OnNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString)
{
	if (IsRunningDedicatedServer() || !ensure(ErrorMessageClass)) { return; }

	UBoiErrorMessage* ErrorMessage = CreateWidget<UBoiErrorMessage>(this, ErrorMessageClass);
	ErrorMessage->SetMessage(ErrorString);
	ErrorMessage->AddToViewport();

}

void UBoiGameInstance::OnTravelFailure(UWorld* World, ETravelFailure::Type FailureType, const FString& ErrorString)
{
	if (IsRunningDedicatedServer() || !ensure(ErrorMessageClass)) { return; }

	UBoiErrorMessage* ErrorMessage = CreateWidget<UBoiErrorMessage>(this, ErrorMessageClass);
	ErrorMessage->SetMessage(ErrorString);
	ErrorMessage->AddToViewport();
}

void UBoiGameInstance::QuitToMainMenu()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();

	if (!ensure(PlayerController)) { return; }

	PlayerController->ClientTravel(MAIN_MENU_MAP_PATH, ETravelType::TRAVEL_Absolute);

	FNamedOnlineSession* ExistingSession = SessionInterface->GetNamedSession(NAME_GameSession);
	if (ExistingSession)
	{
		SessionInterface->DestroySession(NAME_GameSession);
	}
}

void UBoiGameInstance::BeginLoadingScreen(const FString& MapName)
{
	if (!IsRunningDedicatedServer())
	{
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = false;
		LoadingScreen.WidgetLoadingScreen = FLoadingScreenAttributes::NewTestLoadingScreenWidget();
		if (LoadingScreenClass)
		{
			LoadingScreenWidget = CreateWidget<UUserWidget>(this, LoadingScreenClass);
			LoadingScreen.WidgetLoadingScreen = LoadingScreenWidget->TakeWidget();
		}

		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
	}
}

void UBoiGameInstance::EndLoadingScreen(UWorld* InLoadedWorld)
{

}

FString UBoiGameInstance::GetMapName(int MapID) const
{
	return Maps[MapID].InGameName;
}


UTexture2D* UBoiGameInstance::GetMapImage(int MapID) const
{
	return Maps[MapID].MapIcon;
}

void UBoiGameInstance::ShowMainMenu()
{
	if (!ensure(MainMenuClass)) { return; }

	APlayerController* PC = GetFirstLocalPlayerController();
	MainMenu = CreateWidget<UBoiMainMenu>(PC, MainMenuClass);
	MainMenu->Setup();
	MainMenu->SetMenuInterface(this);
}

void UBoiGameInstance::ShowInGameMenu()
{
	if (!ensure(InGameMenuClass)) { return; }

	UMenuWidget* InGameMenu = CreateWidget<UMenuWidget>(this, InGameMenuClass);
	InGameMenu->Setup();
	InGameMenu->SetMenuInterface(this);
}

void UBoiGameInstance::ShowCreateAClassMenu()
{
	if (!ensure(CreateAClassMenuClass)) { return; }

	UMenuWidget* CreateAClassMenu = CreateWidget<UMenuWidget>(this, CreateAClassMenuClass);
	CreateAClassMenu->Setup();
	CreateAClassMenu->SetMenuInterface(this);
}

void UBoiGameInstance::Host(FGameSettings Settings)
{
	if (Settings.MaxPlayerCount < 1)
	{
		UE_LOG(LogTemp, Warning, TEXT("Invalid Player Count"));
		return;
	}
	
	if (!SessionInterface.IsValid()) 
	{
		UE_LOG(LogTemp, Warning, TEXT("Invalid Session Interface"));
		return;
	}

	GameSettings = Settings;
	GameSettings.bInitialized = true;

	FOnlineSessionSettings SessionSettings;
	SessionSettings.NumPublicConnections = GameSettings.MaxPlayerCount;
	SessionSettings.bShouldAdvertise = true;
	SessionSettings.bAllowJoinInProgress = true;
	SessionSettings.bIsLANMatch = (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL");
	SessionSettings.bUsesPresence = true;
	SessionSettings.bAllowJoinViaPresence = true;
	SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, GameSettings.ServerName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

	SessionInterface->CreateSession(0, NAME_GameSession, SessionSettings);
}

void UBoiGameInstance::OnCreateSessionComplete(FName SessionName, bool Success)
{
	if (!Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to create session"));
		return;
	}

	if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, TEXT("Hosting")); }
	
	GetWorld()->ServerTravel("/Game/FPSBois/Maps/Lobby?listen");
}

void UBoiGameInstance::OnDestroySessionComplete(FName SessionName, bool Success)
{

}

void UBoiGameInstance::RefreshServerList()
{
	if (!SessionInterface.IsValid()) { return; }

	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	SessionSearch->bIsLanQuery = (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL");
	SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
	SessionSearch->MaxSearchResults = 100;

	UE_LOG(LogTemp, Warning, TEXT("Starting Find Session"));
	SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
}

void UBoiGameInstance::OnFindSessionsComplete(bool Success)
{
	if (!Success || !SessionSearch.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("Error finding game sessions."));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("Finding game sessions complete. Found %d server(s)."), SessionSearch->SearchResults.Num());

	TArray<FServerData> ServerNames;
	for (const FOnlineSessionSearchResult& Result : SessionSearch->SearchResults)
	{
		FServerData Data;
		Data.Name = Result.GetSessionIdStr();
		Data.MaxPlayers = Result.Session.SessionSettings.NumPublicConnections;
		Data.CurrentPlayers = Data.MaxPlayers - Result.Session.NumOpenPublicConnections;
		Data.HostUsername = Result.Session.OwningUserName;
		FString ServerName;
		if (Result.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName))
		{
			Data.Name = ServerName;
		}
		else
		{
			Data.Name = "N/A";
		}

		ServerNames.Add(Data);
	}

	MainMenu->SetServerList(ServerNames);
}

void UBoiGameInstance::Join(uint32 Index)
{
	if (!SessionInterface.IsValid() || !SessionSearch.IsValid()) { return; }

	SessionInterface->JoinSession(0, NAME_GameSession, SessionSearch->SearchResults[Index]);
}

void UBoiGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	if (!SessionInterface.IsValid()) { return; }

	FString Address;
	if (!SessionInterface->GetResolvedConnectString(SessionName, Address))
	{
		if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, FString::Printf(TEXT("Could not get a connection string"))); }
		UE_LOG(LogTemp, Warning, TEXT("Could not get a connection string"));
		return;
	}

	if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, FString::Printf(TEXT("Joining %s"), *Address)); }

	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController)) { return; }
	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
}

void UBoiGameInstance::QuitToDesktop()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController)) { return; }
	PlayerController->ConsoleCommand(TEXT("Quit"));
}

void UBoiGameInstance::DestroySession(APlayerController* PlayerController)
{
	if (SessionInterface.IsValid())
	{
		SessionInterface->DestroySession(NAME_GameSession);
	}
}

void UBoiGameInstance::StartGame(FGameSettings Settings)
{
	if (!SessionInterface.IsValid()) { return; }

	/// Inform the session that the game is starting
	SessionInterface->StartSession(NAME_GameSession);

	GameSettings = Settings;

	/// Construct the path to the correct map with the correct game mode
	FString Path = "/Game/FPSBois/Maps/";

	if (GameSettings.MapID < Maps.Num()) 
	{
		Path += Maps[GameSettings.MapID].EditorName;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Map Path Name is missing. Check GI blueprint MapEditorNames"));
		return;
	}

	Path += "?Game=";

	switch (GameSettings.GameMode)
	{
	case EGameMode::FFA:
		Path += "FFA";
		break;
	case EGameMode::TDM:
		Path += "TDM";
		break;
	default:
		UE_LOG(LogTemp, Error, TEXT("GameMode doesn't exist."));
		return;
	}
	
	// TODO: include ?listen and option for joining an in progress game

	GetWorld()->ServerTravel(Path);
}
