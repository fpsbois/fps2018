// Copyright © FPSBois 2018

#include "BoiGameMode.h"

#include "GameFramework/PlayerStart.h"
#include "TimerManager.h"
#include "EngineUtils.h"
#include "Engine.h"

#include "BoiGameInstance.h"
#include "BoiGameState.h"
#include "BoiLobbyPC.h"
#include "BoiPlayerCharacter.h"
#include "BoiPlayerController.h"
#include "BoiPlayerState.h"

#include "BoiHealthComponent.h"

#define print(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::White, text)

ABoiGameMode::ABoiGameMode()
{
	DefaultPawnClass = ABoiPlayerCharacter::StaticClass();
	GameStateClass = ABoiGameState::StaticClass();
	PlayerStateClass = ABoiPlayerState::StaticClass();
	PlayerControllerClass = ABoiPlayerController::StaticClass();

	bUseSeamlessTravel = true;
	bPauseable = false;

	ScoreLimit = 10;
	TimeLimit = 120;
	RespawnTime = 5;
	bAllowNegativeScores = false;
	PointsFromSuicide = -1;
	PointsFromKill = 1;
	PointsFromTeamkill = -1;
	PointsFromAssist = 0;
}

void ABoiGameMode::StartPlay()
{
	Super::StartPlay();

	UBoiGameInstance* GameInstance = Cast<UBoiGameInstance>(GetGameInstance());

	if (!ensureAlways(GameInstance)) 
	{ 
		UE_LOG(LogTemp, Error, TEXT("Invalid Game Instance! Check Project Settings."));  
		return; 
	}

	const FGameSettings GameSettings = GameInstance->GetGameSettings();
	if (GameSettings.bInitialized)
	{
		ScoreLimit = GameSettings.ScoreLimit;
		TimeLimit = GameSettings.TimeLimit;
	}

	StartGame();
}

// Only gets called on new connections, meaning if seamless travel is on then this won't get called on the new map because the connections already exist
void ABoiGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	ABoiPlayerState* State = Cast<ABoiPlayerState>(NewPlayer->PlayerState);

	if (!ensure(State))
	{
		UE_LOG(LogTemp, Error, TEXT("PlayerState class must inherit from ABoiPlayerState. Check GameMode settings."));
	}
	else
	{
		
		AssignNewPlayerToTeam(NewPlayer, State);
	}

	AllPlayerControllers.Add(NewPlayer);
}

void ABoiGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetGameInstance());
	if (GI)
	{
		// This might destroy all sessions and not just the one for the player that is leaving. Maybe need a client method that calls this instead.
		GI->DestroySession(nullptr);
	}
}

// Only gets called when seamless travel is on and the default player controllers are different on the two game modes of the two maps
void ABoiGameMode::SwapPlayerControllers(APlayerController* OldPC, APlayerController* NewPC)
{
	Super::SwapPlayerControllers(OldPC, NewPC);
	ABoiPlayerController* PC = Cast<ABoiPlayerController>(NewPC);
	PC->Client_ClearHUDWidgets();
	AllPlayerControllers.Add(NewPC); 
}

void ABoiGameMode::RespawnPlayerAtPlayerStartWithTimer(ABoiPlayerController* PlayerController, APlayerStart* PlayerStart, float AfterXSeconds)
{
	FTimerDelegate TimerDel;
	FTimerHandle TimerHandle_SpawnTimer;
	TimerDel.BindUFunction(this, FName("RespawnPlayerAtPlayerStart"), PlayerController, PlayerStart);
	GetWorldTimerManager().SetTimer(TimerHandle_SpawnTimer, TimerDel, AfterXSeconds, false);
	print(TEXT("Spawn Timer Started"));
}

bool ABoiGameMode::DidKillFriendly(AActor* VictimActor, AController* VictimController, AActor* KillerActor, AController* KillerController) const
{
	if (!VictimController || !KillerController) { return false; }

	auto VictimState = Cast<ABoiPlayerState>(VictimController->PlayerState);
	auto KillerState = Cast<ABoiPlayerState>(KillerController->PlayerState);

	if (!VictimState || !KillerState) { return false; }

	return VictimState->GetTeam() == KillerState->GetTeam();
}

bool ABoiGameMode::DidKillSelf(AActor* VictimActor, AController* VictimController, AActor* KillerActor, AController* KillerController) const
{
	if (!VictimController || !KillerController) { return false; }

	return VictimController == KillerController;
}

void ABoiGameMode::RespawnPlayerAtPlayerStart(ABoiPlayerController* PlayerController, APlayerStart* PlayerStart)
{
	print(TEXT("Spawn Timer Ended"));
	RestartPlayerAtPlayerStart(PlayerController, PlayerStart);
}

void ABoiGameMode::StartGame()
{
	OnActorKilled.AddDynamic(this, &ABoiGameMode::ActorKilled);
	
	ABoiGameState* GS = GetGameState<ABoiGameState>();
	if (ensureAlways(GS))
	{
		OnActorKilled.AddDynamic(GS, &ABoiGameState::MulticastActorKilled);
		GS->TimeRemaining = TimeLimit;
		GS->OnTimeRemainingChanged.Broadcast();
		GS->ScoreLimit = ScoreLimit;
	}
	
	GetWorldTimerManager().SetTimer(TimerHandle_GameTimerTick, this, &ABoiGameMode::OnGameTimerTick, 1, true);
}

void ABoiGameMode::EndGame()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_GameTimerTick);
	GetWorldTimerManager().ClearTimer(TimerHandle_PlayerSpawner);

	ABoiGameState* GS = GetGameState<ABoiGameState>();
	if (GS)
	{
		GS->MulticastOnEndRound();
	}
}

void ABoiGameMode::AddPlayerBalanced(APlayerController* NewPlayerController, ABoiPlayerState* NewPlayerState)
{
	ABoiGameState* State = GetGameState<ABoiGameState>();

	if (!ensure(State))
	{
		return;
	}

	if (State->GetTeamSize(0) <= State->GetTeamSize(1))
	{
		State->AddPlayerToTeam(NewPlayerController, 0);
	}
	else
	{
		State->AddPlayerToTeam(NewPlayerController, 1);
	}
}

void ABoiGameMode::StartSpawnFromQueueTimer()
{
	FTimerHandle TimerHandle_SpawnTimer;	
	GetWorldTimerManager().SetTimer(TimerHandle_SpawnTimer, this, &ABoiGameMode::SpawnPlayerFromQueue, RespawnTime, false);
}

void ABoiGameMode::SpawnPlayerFromQueue()
{
	TArray<APlayerStart*> PlayerStarts;
	for (TActorIterator<APlayerStart> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		APlayerStart* PlayerStart = *ActorItr;
		PlayerStarts.Add(PlayerStart);
	}

	uint8 Index = FMath::RandRange(0, PlayerStarts.Num()-1);
	APlayerStart* PS = PlayerStarts[Index];

	if (DeadPlayerQueue.Num() > 0) 
	{
		AController* PC = DeadPlayerQueue[0];
		RestartPlayerAtPlayerStart(PC, PS);
		DeadPlayerQueue.RemoveAt(0);
	}

}

void ABoiGameMode::OnGameTimerTick()
{
	ABoiGameState* GS = GetGameState<ABoiGameState>();
	if (ensureAlways(GS))
	{
		GS->TimeRemaining--;

		if (GS->TimeRemaining <= 0)
		{
			EndGame();
		}
		GS->OnTimeRemainingChanged.Broadcast();
	}

}

