// Copyright FPSBois 2018

#include "BoiGameState.h"

#include "Engine/World.h"
#include "Net/UnrealNetwork.h"

#include "BoiPlayerState.h"
#include "BoiHealthComponent.h"
#include "BoiPlayerController.h"

ABoiGameState::ABoiGameState()
{
	TeamSizes.SetNumZeroed(2, false);
}

int32 ABoiGameState::AddScoreToPlayersTeam(AController* Controller, int32 DeltaScore)
{
	if (!Controller) { return 0; }

	auto PlayerState = Cast<ABoiPlayerState>(Controller->PlayerState);

	if (!PlayerState) { return 0; }

	uint8 Team = PlayerState->GetTeam();

	if (Role == ROLE_Authority)
	{
		if (TeamScores.Num() > Team)
		{
			TeamScores[Team] += DeltaScore;
		}
		else
		{
			TeamScores.SetNumZeroed(Team + 1, false);
			TeamScores[Team] += DeltaScore;
		}

		if (TeamScores[Team] < 0)
		{
			TeamScores[Team] = 0;
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Team %d has %d points"), Team, TeamScores[Team]);

	OnScoreChanged.Broadcast();

	return DeltaScore;
}

void ABoiGameState::AddScore(uint8 Team, int32 DeltaScore)
{
	if (Role == ROLE_Authority)
	{
		if (TeamScores.Num() > Team)
		{
			TeamScores[Team] += DeltaScore;
		}
		else
		{
			TeamScores.SetNumZeroed(Team + 1, false);
			TeamScores[Team] += DeltaScore;
		}

		if (TeamScores[Team] < 0)
		{
			TeamScores[Team] = 0;
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Team %d has %d points"), Team, TeamScores[Team]);

	OnScoreChanged.Broadcast();
}

void ABoiGameState::AddTime(int32 Time)
{
	if (HasAuthority())
	{
		TimeLimit += Time;
	}
}

void ABoiGameState::MulticastOnPlayerTeamChanged_Implementation()
{
	OnPlayerTeamChanged.Broadcast();
}

void ABoiGameState::AddPlayerToTeam(AController* Controller, uint8 Team)
{
	ABoiPlayerState* PlayerState = Cast<ABoiPlayerState>(Controller->PlayerState);
	if (PlayerState)
	{
		PlayerState->SetTeam(Team);
	}

	if (TeamSizes.Num() > Team)
	{
		TeamSizes[Team]++;
	}
	else
	{
		TeamSizes.SetNumZeroed(Team + 1, false);
		TeamSizes[Team]++;
	}

	// If the controlled pawn has a health component, update the team on it.
	APawn* ControlledPawn = Controller->GetPawn();
	if (ControlledPawn)
	{
		auto HealthComp = Cast<UBoiHealthComponent>(ControlledPawn->GetComponentByClass(UBoiHealthComponent::StaticClass()));
		if (HealthComp) 
		{
			HealthComp->SetTeam(Team);
		}
	}

	MulticastOnPlayerTeamChanged();
}

int32 ABoiGameState::GetTeamSize(int32 Team)
{
	return TeamSizes[Team];
}

void ABoiGameState::MulticastActorKilled_Implementation(AActor* VictimActor, AController* VictimController, AActor* KillerActor, AController* KillerController)
{
	OnActorKilled.Broadcast(VictimActor, VictimController, KillerActor, KillerController);
}

void ABoiGameState::MulticastOnEndRound_Implementation()
{
	// Server will have ALL controllers, clients will only have one
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; It++)
	{
		ABoiPlayerController* Controller = Cast<ABoiPlayerController>(It->Get());
		// because the server has all PCs, this is to check if it is controlled locally.
		if (Controller && Controller->IsLocalController())
		{
			Controller->OnRoundComplete();

			APawn* MyPawn = Controller->GetPawn();
			if (MyPawn)
			{
				// This is not a replicated function, so should only be called on locally controlled players.
				MyPawn->DisableInput(Controller);
			}
		}
	}
}

void ABoiGameState::OnRep_TeamScoreChanged()
{
	OnScoreChanged.Broadcast();
}

void ABoiGameState::OnRep_TimeRemainingChanged()
{
	OnTimeRemainingChanged.Broadcast();
}

void ABoiGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABoiGameState, TeamScores);
	DOREPLIFETIME(ABoiGameState, TimeRemaining);
	DOREPLIFETIME(ABoiGameState, TimeLimit);
	DOREPLIFETIME(ABoiGameState, ScoreLimit);
	DOREPLIFETIME(ABoiGameState, GameMode);
	DOREPLIFETIME(ABoiGameState, MaxPlayerCount);
}