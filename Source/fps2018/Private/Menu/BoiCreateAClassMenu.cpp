// Copyright FPSBois 2018

#include "BoiCreateAClassMenu.h"

#include "Kismet/GameplayStatics.h"

#include "BoiCreateAClassSave.h"

UBoiCreateAClassMenu::UBoiCreateAClassMenu()
{
	Slot1Name = "CustomClass1";
	Slot2Name = "CustomClass2";
	Slot3Name = "CustomClass3";
}

bool UBoiCreateAClassMenu::LoadSavedClasses()
{
	UBoiCreateAClassSave* SG1 = Cast<UBoiCreateAClassSave>(UGameplayStatics::LoadGameFromSlot(Slot1Name, 0));
	UBoiCreateAClassSave* SG2 = Cast<UBoiCreateAClassSave>(UGameplayStatics::LoadGameFromSlot(Slot2Name, 0));
	UBoiCreateAClassSave* SG3 = Cast<UBoiCreateAClassSave>(UGameplayStatics::LoadGameFromSlot(Slot3Name, 0));

	if (SG1) { SaveSlot1 = SG1; }
	if (SG2) { SaveSlot2 = SG2; }
	if (SG3) { SaveSlot3 = SG3; }

	return SG1 && SG2 && SG3;
}

void UBoiCreateAClassMenu::CreateEmptySavedClassess()
{
	if (!ensure(CreateAClassSaveClass)) { return; }

	SaveSlot1 = Cast<UBoiCreateAClassSave>(UGameplayStatics::CreateSaveGameObject(CreateAClassSaveClass));
	SaveSlot2 = Cast<UBoiCreateAClassSave>(UGameplayStatics::CreateSaveGameObject(CreateAClassSaveClass));
	SaveSlot3 = Cast<UBoiCreateAClassSave>(UGameplayStatics::CreateSaveGameObject(CreateAClassSaveClass));
}

void UBoiCreateAClassMenu::NativePreConstruct()
{
	Super::NativePreConstruct();

	bool bLoadUnsuccesful = !LoadSavedClasses();
	if (bLoadUnsuccesful)
	{
		CreateEmptySavedClassess();

		SaveSlot1->PrimaryWeaponIndex = 0;
		SaveSlot1->SecondaryWeaponIndex = 0;

		SaveSlot2->PrimaryWeaponIndex = 1;
		SaveSlot2->SecondaryWeaponIndex = 1;

		SaveSlot3->PrimaryWeaponIndex = 2;
		SaveSlot3->SecondaryWeaponIndex = 0;
		
		SaveAllClasses();
	}
}

void UBoiCreateAClassMenu::SaveAllClasses()
{
	if (!ensure(CreateAClassSaveClass)) { return; }

	if (!SaveSlot1) { SaveSlot1 = Cast<UBoiCreateAClassSave>(UGameplayStatics::CreateSaveGameObject(CreateAClassSaveClass)); }
	if (!SaveSlot2) { SaveSlot2 = Cast<UBoiCreateAClassSave>(UGameplayStatics::CreateSaveGameObject(CreateAClassSaveClass)); }
	if (!SaveSlot3) { SaveSlot3 = Cast<UBoiCreateAClassSave>(UGameplayStatics::CreateSaveGameObject(CreateAClassSaveClass)); }

	UGameplayStatics::SaveGameToSlot(SaveSlot1, Slot1Name, 0);
	UGameplayStatics::SaveGameToSlot(SaveSlot2, Slot2Name, 0);
	UGameplayStatics::SaveGameToSlot(SaveSlot3, Slot3Name, 0);
}