// Fill out your copyright notice in the Description page of Project Settings.

#include "BoiInGameMenu.h"

#include "GameFramework/GameModeBase.h"
#include "Components/Button.h"
#include "GameFramework/PlayerController.h"

bool UBoiInGameMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) { return false; }

	if (!ensure(MainMenuButton)) { return false; }
	MainMenuButton->OnClicked.AddDynamic(this, &UBoiInGameMenu::QuitToMainMenu);

	if (!ensure(ResumeButton)) { return false; }
	ResumeButton->OnClicked.AddDynamic(this, &UBoiInGameMenu::ResumeGame);

	if (!ensure(QuitButton)) { return false; }
	QuitButton->OnClicked.AddDynamic(this, &UBoiInGameMenu::QuitGame);

	return true;
}

void UBoiInGameMenu::ResumeGame()
{
	Teardown();
}

void UBoiInGameMenu::QuitToMainMenu()
{
	APlayerController* PlayerController = GetOwningPlayer();
	if (!ensure(PlayerController != nullptr)) return;

	UWorld* World = PlayerController->GetWorld();
	if (!ensure(World != nullptr)) return;

	if (World->IsServer()) {
		AGameModeBase* GameMode = World->GetAuthGameMode<AGameModeBase>();
		if (GameMode) {
			GameMode->ReturnToMainMenuHost();
		}
	}
	else {

		if (PlayerController) {
			FText Reason;
			PlayerController->ClientReturnToMainMenuWithTextReason(Reason);
		}
	}
}

void UBoiInGameMenu::QuitGame()
{
	if (MenuInterface)
	{
		MenuInterface->QuitToDesktop();
	}
}