// Copyright © FPSBois 2018

#include "BoiLobbyGM.h"

#include "Net/UnrealNetwork.h"

#include "BoiEnumsStructs.h"
#include "BoiGameInstance.h"
#include "BoiLobbyPC.h"



ABoiLobbyGM::ABoiLobbyGM()
{
	PlayerControllerClass = ABoiLobbyPC::StaticClass();

	// Set Default Game Settings
	GameSettings.GameMode = EGameMode::FFA;
	GameSettings.ScoreLimit = 1500;
	GameSettings.TimeLimit = 600;
	GameSettings.MapID = 0;
	GameSettings.ServerName = "FPS Bois Game";
	GameSettings.MaxPlayerCount = 4;

	bUseSeamlessTravel = true;
}

void ABoiLobbyGM::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	// Get Game Info From Game Instance
	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetGameInstance());
	if (!ensure(GI)) { return; }
	if (GI->GetGameSettings().bInitialized)
	{
		GameSettings.ServerName = GI->GetGameSettings().ServerName;
		GameSettings.MaxPlayerCount = GI->GetGameSettings().MaxPlayerCount;
	}
}

void ABoiLobbyGM::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	AllPlayerControllers.Add(NewPlayer);

	// Balance Teams
	uint8 TeamOne = 0;
	uint8 TeamTwo = 0;
	for (int32 i = 0; i != ConnectedPlayersInfo.Num(); ++i)
	{
		((ConnectedPlayersInfo[i].Team == (uint8)0) ? TeamOne : TeamTwo) += 1;
	}
	uint8 Team = (TeamOne <= TeamTwo) ? 0 : 1;

	// Initialize New PlayerController
	ABoiLobbyPC* PC = Cast<ABoiLobbyPC>(NewPlayer);
	if (!ensure(PC)) { return; }
	PC->Client_SetupLobbyMenu(FText::FromString(GameSettings.ServerName));
	PC->Client_UpdateLobbySettings(GameSettings.MapID, GameSettings.TimeLimit, GameSettings.ScoreLimit, GameSettings.GameMode);
	PC->Client_InitialSetup(Team);
}

void ABoiLobbyGM::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	APlayerController* PC = Cast<APlayerController>(Exiting);
	if (PC)
	{
		AllPlayerControllers.Remove(PC);
	}

	UpdateAllClients();
}

void ABoiLobbyGM::ChangeGameSettings(int NewMapTime, int NewMapID, int NewScoreLimit, EGameMode NewGameMode)
{
	GameSettings.TimeLimit = NewMapTime;
	GameSettings.MapID = NewMapID;
	GameSettings.ScoreLimit = NewScoreLimit;
	GameSettings.GameMode = NewGameMode;

	// TODO: Setup other game options here

	// Inform each client to update lobby info with changes
	for (APlayerController* PlayerController : AllPlayerControllers)
	{
		ABoiLobbyPC* PC = Cast<ABoiLobbyPC>(PlayerController);
		PC->Client_UpdateLobbySettings(NewMapID, NewMapTime, NewScoreLimit, NewGameMode);
	}
}

void ABoiLobbyGM::StartGame()
{
	// Destroy Lobby Widgets
	for (APlayerController* PlayerController : AllPlayerControllers)
	{
		ABoiLobbyPC* PC = Cast<ABoiLobbyPC>(PlayerController);
		PC->Client_Teardown();
		PC->Client_SetupLoadingScreen();
	}

	GameSettings.bInitialized = true;
	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetGameInstance());
	GI->StartGame(GameSettings);

}

void ABoiLobbyGM::UpdateAllClients()
{
	int32 CurrentPlayers = AllPlayerControllers.Num();

	if (CurrentPlayers > 0)
	{
		ConnectedPlayersInfo.Empty();
		
		for (APlayerController* PlayerController : AllPlayerControllers)
		{
			ABoiLobbyPC* PC = Cast<ABoiLobbyPC>(PlayerController);
			ConnectedPlayersInfo.Add(PC->PlayerSettings);
			PC->Client_UpdateNumberOfPlayers(CurrentPlayers, GameSettings.MaxPlayerCount);
		}

		for (APlayerController* PlayerController : AllPlayerControllers)
		{
			ABoiLobbyPC* PC = Cast<ABoiLobbyPC>(PlayerController);
			PC->Client_AddPlayerInfo(ConnectedPlayersInfo);

			// If we want to restrict players from playing the same class, 
			// call PC->Client_UpdateAvailableCharacters(AvailableCharacters);
		}

		bCanStartGame = true;
		// Check if players are ready, if not set it to false
		/*for (const FPlayerInfo& PlayerInfo: ConnectedPlayers)
		{
			// if not ready, set bCanStartGame to false and break;
		}*/
	}
}