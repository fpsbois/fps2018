// Copyright © FPSBois 2018

#include "BoiLobbyMenu.h"

#include "Net/UnrealNetwork.h"
#include "Components/ScrollBox.h"

#include "BoiGameInstance.h"
#include "BoiLobbyGM.h"
#include "BoiLobbyPC.h"
#include "BoiPlayerCard.h"



void UBoiLobbyMenu::NativeConstruct()
{
	Super::NativeConstruct();

	LocalController = Cast<ABoiLobbyPC>(GetOwningPlayer());

	if (!ensure(LocalController))
	{ 
		UE_LOG(LogTemp, Warning, TEXT("Invalid PlayerController. Must inherit from ABoiLobbyPC."));
		return; 
	}

	LocalController->PlayerSettings.PlayerStatus = (LocalController->HasAuthority() ? FText::FromString("Host") : FText::FromString("Not Ready!"));
}

void UBoiLobbyMenu::ClearPlayerLists()
{
	TeamOneList->ClearChildren();
	TeamTwoList->ClearChildren();
}

void UBoiLobbyMenu::StartGame()
{
	if (GetOwningPlayer() && GetOwningPlayer()->HasAuthority())
	{
		ABoiLobbyGM* GM = Cast<ABoiLobbyGM>(GetWorld()->GetAuthGameMode());
		if (!GM) { return; }
		GM->StartGame();
	}
}

void UBoiLobbyMenu::ReadyUp()
{
	if (!LocalController) { return; }

	bReady = !bReady;
	ReadyStatus = (bReady ? FText::FromString("Ready!") : FText::FromString("Not Ready!"));

	LocalController->PlayerSettings.PlayerStatus = ReadyStatus;
	LocalController->Server_CallUpdate(LocalController->PlayerSettings);
}

void UBoiLobbyMenu::SwapTeams()
{
	if (!LocalController) { return; }

	uint8 T = LocalController->PlayerSettings.Team;
	LocalController->PlayerSettings.Team = (T == 0 ? 1 : 0);
	LocalController->Server_CallUpdate(LocalController->PlayerSettings);
}

void UBoiLobbyMenu::LeaveGame()
{
	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetOwningPlayer()->GetGameInstance());
	GI->QuitToMainMenu();
}

void UBoiLobbyMenu::Client_AddPlayerCard_Implementation(FPlayerInfo PlayerInfo)
{
	if (!PlayerCardClass)
	{
		UE_LOG(LogTemp, Error, TEXT("Missing PlayerCardClass. Check LobbyMenu Default Properties."))
		return;
	}

	UBoiPlayerCard* PlayerCard = CreateWidget<UBoiPlayerCard>(GetOwningPlayer(), PlayerCardClass);
	PlayerCard->Setup(PlayerInfo);

	if (PlayerInfo.Team == 0)
	{
		TeamOneList->AddChild(PlayerCard);
	}
	else
	{
		TeamTwoList->AddChild(PlayerCard);
	}

}

void UBoiLobbyMenu::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBoiLobbyMenu, LobbyServerName);
	DOREPLIFETIME(UBoiLobbyMenu, MapName);
	DOREPLIFETIME(UBoiLobbyMenu, TimeLimit);
	DOREPLIFETIME(UBoiLobbyMenu, MapImage);
	DOREPLIFETIME(UBoiLobbyMenu, CurrentPlayers);
	DOREPLIFETIME(UBoiLobbyMenu, MaxPlayers);
	DOREPLIFETIME(UBoiLobbyMenu, ReadyButtonText);
	DOREPLIFETIME(UBoiLobbyMenu, ReadyStatus);
	DOREPLIFETIME(UBoiLobbyMenu, ScoreLimit);
	DOREPLIFETIME(UBoiLobbyMenu, MapID);
	DOREPLIFETIME(UBoiLobbyMenu, GameMode);
}