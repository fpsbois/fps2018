// Copyright FPSBois 2018

#include "BoiLobbyPC.h"

#include "BoiGameInstance.h"
#include "BoiLobbyGM.h"
#include "BoiLobbyMenu.h"

#include "Blueprint/UserWidget.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
//#include "ThirdParty/Steamworks/Steamv139/sdk/public/steam_api.h"
#include "UObject/ConstructorHelpers.h"
#include "TimerManager.h"

#define LOCTEXT_NAMESPACE "Lobby"

ABoiLobbyPC::ABoiLobbyPC()
{
	PlayerSettingsSaveSlotName = "PlayerSettingsSave";

	ConstructorHelpers::FClassFinder<UBoiPlayerSaveGame> SaveGameBPClass(PLAYER_SAVE_GAME_PATH);
	if (!ensure(SaveGameBPClass.Class != nullptr)) { return; }
	SaveGameClass = SaveGameBPClass.Class;
}

void ABoiLobbyPC::BeginPlay()
{
	Super::BeginPlay();

	// Sometimes PlayerState is not initialized yet, so we set a short timer here to check again later.
	if (PlayerState) {
		PlayerSettings.Name = FText::FromString(PlayerState->GetPlayerName());
		UTexture2D* SteamAvatar = GetSteamAvatar();
		if (SteamAvatar)
		{
			PlayerSettings.PlayerImage = SteamAvatar;
		}
		Server_CallUpdate(PlayerSettings);
	}
	else
	{
		FTimerHandle CheckForNameAgain;
		GetWorldTimerManager().SetTimer(CheckForNameAgain, this, &ABoiLobbyPC::UpdateName, .5f, true);
	}
}

void ABoiLobbyPC::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	// TODO: Display image or main menu something about lost connection.

	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetGameInstance());
	if (GI)
	{
		GI->DestroySession(this);
	}
}

UTexture2D* ABoiLobbyPC::GetSteamAvatar() 
{
	// TODO: Figure out how to Get Steam Avatar

	//uint32 Width;
	//uint32 Height;

	//if (SteamAPI_IsSteamRunning())
	//{
	//	//Getting the PictureID from the SteamAPI and getting the Size with the ID
	//	int Picture = SteamFriends()->GetMediumFriendAvatar(SteamUser()->GetSteamID());
	//	SteamUtils()->GetImageSize(Picture, &Width, &Height);


	//	if (Width > 0 && Height > 0)
	//	{
	//		//Creating the buffer "oAvatarRGBA" and then filling it with the RGBA Stream from the Steam Avatar
	//		BYTE *oAvatarRGBA = new BYTE[Width * Height * 4];


	//		//Filling the buffer with the RGBA Stream from the Steam Avatar and creating a UTextur2D to parse the RGBA Steam in
	//		SteamUtils()->GetImageRGBA(Picture, (uint8*)oAvatarRGBA, 4 * Height * Width * sizeof(char));

	//		UTexture2D* Avatar = UTexture2D::CreateTransient(Width, Height, PF_R8G8B8A8);

	//		//MAGIC!
	//		uint8* MipData = (uint8*)Avatar->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
	//		FMemory::Memcpy(MipData, (void*)oAvatarRGBA, Height * Width * 4);
	//		Avatar->PlatformData->Mips[0].BulkData.Unlock();

	//		delete oAvatarRGBA;

	//		//Setting some Parameters for the Texture and finally returning it
	//		Avatar->PlatformData->NumSlices = 1;
	//		Avatar->NeverStream = true;
	//		//Avatar->CompressionSettings = TC_EditorIcon;

	//		Avatar->UpdateResource();

	//		return Avatar;
	//	}
	//	return nullptr;
	//}
	return nullptr;
}

void ABoiLobbyPC::UpdateName()
{
	PlayerSettings.Name = FText::FromString(PlayerState->GetPlayerName());
	UTexture2D* SteamAvatar = GetSteamAvatar();
	if (SteamAvatar)
	{
		PlayerSettings.PlayerImage = SteamAvatar;
	}
	Server_CallUpdate(PlayerSettings);
}

void ABoiLobbyPC::Client_Teardown_Implementation()
{
	LobbyMenuWidget->Teardown();
}

void ABoiLobbyPC::Client_InitialSetup_Implementation(uint8 Team)
{
	// Initialize Player Settings
	SaveGameCheck();
	PlayerSettings.Team = Team;
	Server_CallUpdate(PlayerSettings);
}

void ABoiLobbyPC::Client_SetupLobbyMenu_Implementation(const FText& ServerName)
{
	// Create the Lobby Widget
	if (!ensure(LobbyMenuClass)) { return; }
	LobbyMenuWidget = CreateWidget<UBoiLobbyMenu>(GetWorld(), LobbyMenuClass);
	LobbyMenuWidget->LobbyServerName = ServerName;
	LobbyMenuWidget->Setup();
}

void ABoiLobbyPC::Client_SetupLoadingScreen_Implementation()
{
	if (!ensure(LoadingScreenClass)) { return; }
	UUserWidget* LoadScreen = CreateWidget<UUserWidget>(this, LoadingScreenClass);
	LoadScreen->AddToViewport();
}

void ABoiLobbyPC::Server_CallUpdate_Implementation(FPlayerInfo PlayerInfo)
{
	PlayerSettings = PlayerInfo;
	ABoiLobbyGM* GM = Cast<ABoiLobbyGM>(GetWorld()->GetAuthGameMode());
	if (!ensure(GM)) { return; }
	GM->UpdateAllClients();
}

bool ABoiLobbyPC::Server_CallUpdate_Validate(FPlayerInfo PlayerInfo)
{
	return true;
}

void ABoiLobbyPC::Client_UpdateLobbySettings_Implementation(int MapID, int MapTime, int ScoreLimit, EGameMode GameMode)
{
	// Update LobbyMenuWidget Game Settings
	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetGameInstance());
	if (!ensure(GI)) { return; }

	LobbyMenuWidget->MapImage = GI->GetMapImage(MapID);
	LobbyMenuWidget->MapName = FText::FromString(GI->GetMapName(MapID));
	LobbyMenuWidget->MapID = MapID;
	LobbyMenuWidget->TimeLimit = MapTime;
	LobbyMenuWidget->ScoreLimit = ScoreLimit;
	LobbyMenuWidget->GameMode = GameMode;
}

void ABoiLobbyPC::Client_Kick_Implementation()
{
	// TODO: Load main menu or display something about being kicked.
	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetGameInstance());
	if (GI)
	{
		GI->DestroySession(this);
	}
}

void ABoiLobbyPC::SaveGameCheck()
{
	UBoiPlayerSaveGame* SG = Cast<UBoiPlayerSaveGame>(UGameplayStatics::LoadGameFromSlot(PlayerSettingsSaveSlotName, 0));

	if (SG)
	{
		LoadGame();
		SaveGame();
	}
	else
	{
		SaveGame();
	}
}

void ABoiLobbyPC::SaveGame()
{
	if (!PlayerSaveGame)
	{
		PlayerSaveGame = Cast<UBoiPlayerSaveGame>(UGameplayStatics::CreateSaveGameObject(SaveGameClass));
	}

	PlayerSaveGame->PlayerInfo = PlayerSettings;
	UGameplayStatics::SaveGameToSlot(PlayerSaveGame, PlayerSettingsSaveSlotName, 0);
}

void ABoiLobbyPC::LoadGame()
{
	PlayerSaveGame = Cast<UBoiPlayerSaveGame>(UGameplayStatics::LoadGameFromSlot(PlayerSettingsSaveSlotName, 0));
	// PlayerSettings.Name = PlayerSaveGame->PlayerInfo.Name;
	// PlayerSettings.PlayerImage = PlayerSaveGame->PlayerInfo.PlayerImage;

	// TODO: Load player settings (create a class) from player save game
}

void ABoiLobbyPC::Client_AddPlayerInfo_Implementation(const TArray<FPlayerInfo>& ConnectedPlayersInfo)
{
	AllConnectedPlayers = ConnectedPlayersInfo;

	if (!ensure(LobbyMenuWidget))
	{
		return;
	}

	LobbyMenuWidget->ClearPlayerLists();

	for (const FPlayerInfo& PlayerInfo : AllConnectedPlayers)
	{
		LobbyMenuWidget->Client_AddPlayerCard(PlayerInfo);
	}
	
}

void ABoiLobbyPC::Client_UpdateNumberOfPlayers_Implementation(int CurrentPlayers, int MaxPlayers)
{
	// Update LobbyMenuWidget Player Counts
	if (!ensure(LobbyMenuWidget)) { return; }
	LobbyMenuWidget->MaxPlayers = MaxPlayers;
	LobbyMenuWidget->CurrentPlayers = CurrentPlayers;
}

void ABoiLobbyPC::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABoiLobbyPC, LobbyMenuWidget);
	DOREPLIFETIME(ABoiLobbyPC, PlayerSettings);
	DOREPLIFETIME(ABoiLobbyPC, AllConnectedPlayers);
}