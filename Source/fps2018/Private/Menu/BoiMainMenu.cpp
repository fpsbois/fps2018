// Copyright � FPSBois 2018

#include "BoiMainMenu.h"

#include "UObject/ConstructorHelpers.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"
#include "Components/ComboBoxString.h"

#include "BoiGameInstance.h"
#include "ServerRow.h"

UBoiMainMenu::UBoiMainMenu(const FObjectInitializer &ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> ServerRowBP(TEXT("/Game/FPSBois/UI/Menus/WBP_ServerRow"));
	if (!ensure(ServerRowBP.Class)) { return; }
	ServerRowClass = ServerRowBP.Class;
}

bool UBoiMainMenu::Initialize()
{
	bool Success = Super::Initialize();

	if (!Success ||
		!ensure(HostButton) ||
		!ensure(HostMenuHostButton) ||
		!ensure(HostMenuCancelButton) ||
		!ensure(JoinButton) ||
		!ensure(JoinMenuBackButton) ||
		!ensure(JoinMenuSearchButton) ||
		!ensure(QuitButton)) 
	{ return false; }

	HostButton->OnClicked.AddDynamic(this, &UBoiMainMenu::OpenHostMenu);
	HostMenuHostButton->OnClicked.AddDynamic(this, &UBoiMainMenu::HostServer);
	HostMenuCancelButton->OnClicked.AddDynamic(this, &UBoiMainMenu::OpenMainMenu);
	JoinButton->OnClicked.AddDynamic(this, &UBoiMainMenu::OpenJoinMenu);
	JoinMenuBackButton->OnClicked.AddDynamic(this, &UBoiMainMenu::OpenMainMenu);
	JoinMenuSearchButton->OnClicked.AddDynamic(this, &UBoiMainMenu::JoinServer);
	QuitButton->OnClicked.AddDynamic(this, &UBoiMainMenu::QuitGame);

	return true;
}

void UBoiMainMenu::HostServer()
{
	if (PlayerCount < 1)
	{
		UE_LOG(LogTemp, Warning, TEXT("Unable to host game with less than 1 player."));
		return;
	}

	if (!MenuInterface)
	{
		UE_LOG(LogTemp, Warning, TEXT("Unable to host game without a MenuInterface."));
		return;
	}

	FGameSettings GameSettings;
	GameSettings.ServerName = ServerNameTextBox->Text.ToString();
	GameSettings.MaxPlayerCount = PlayerCount;

	MenuInterface->Host(GameSettings);
}

void UBoiMainMenu::OpenJoinMenu()
{
	if (!ensure(MenuSwitcher && JoinMenu)) { return; }

	MenuSwitcher->SetActiveWidget(JoinMenu);
	
	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetOwningPlayer()->GetGameInstance());
	if (!ensure(GI)) { return; }
	GI->RefreshServerList();
	SearchingThrobber->SetVisibility(ESlateVisibility::Visible);
}

void UBoiMainMenu::OpenMainMenu()
{
	if (!ensure(MenuSwitcher)) { return; }
	if (!ensure(MainMenu)) { return; }

	MenuSwitcher->SetActiveWidget(MainMenu);
}

void UBoiMainMenu::OpenHostMenu()
{
	if (!ensure(HostMenu && MenuSwitcher)) { return; }

	MenuSwitcher->SetActiveWidget(HostMenu);
}

void UBoiMainMenu::SetServerList(TArray<FServerData> ServerInfo)
{
	if (!ensure(ServerList && ServerRowClass && GetWorld())) { return; }
	SearchingThrobber->SetVisibility(ESlateVisibility::Hidden);
	ServerList->ClearChildren();

	uint32 i = 0;
	for (const FServerData& ServerData : ServerInfo)
	{
		UServerRow* Row = CreateWidget<UServerRow>(GetWorld(), ServerRowClass);
		FString CapacityText = FString::Printf(TEXT("%d/%d"), ServerData.CurrentPlayers, ServerData.MaxPlayers);
		Row->ServerName->SetText(FText::FromString(ServerData.Name));
		Row->HostName->SetText(FText::FromString(ServerData.HostUsername));
		Row->ServerCapacity->SetText(FText::FromString(CapacityText));
		Row->Setup(this, i);
		++i;

		ServerList->AddChild(Row);
	}
}

void UBoiMainMenu::JoinServer()
{
	if (MenuInterface && SelectedIndex.IsSet())
	{
		UE_LOG(LogTemp, Warning, TEXT("Selected Index is %d"), SelectedIndex.GetValue());
		MenuInterface->Join(SelectedIndex.GetValue());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Selected Index not set"));
	}
}

void UBoiMainMenu::QuitGame()
{
	if (MenuInterface)
	{
		MenuInterface->QuitToDesktop();
	}
}

void UBoiMainMenu::UpdateServerListSelection()
{
	for (int32 i = 0; i < ServerList->GetChildrenCount(); ++i)
	{
		auto Row = Cast<UServerRow>(ServerList->GetChildAt(i));
		if (!Row) { continue; }
		if (SelectedIndex.IsSet() && SelectedIndex.GetValue() == i)
		{
			Row->SetColorAndOpacity(ServerListSelectedColor);
		}
		else
		{
			Row->SetColorAndOpacity(ServerListDefaultColor);
		}
	}
}

void UBoiMainMenu::SelectIndex(uint32 Index)
{
	SelectedIndex = Index;
	UpdateServerListSelection();
}