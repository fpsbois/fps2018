// Copyright © FPSBois 2018

#include "BoiPlayerCard.h"

#include "Net/UnrealNetwork.h"


void UBoiPlayerCard::Setup(FPlayerInfo PlayerSettings)
{
	PlayerName = PlayerSettings.Name;
	PlayerStatus = PlayerSettings.PlayerStatus;
	if (PlayerSettings.PlayerImage)
	{
		PlayerImageTexture = PlayerSettings.PlayerImage;
	}
}

void UBoiPlayerCard::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBoiPlayerCard, PlayerName);
	DOREPLIFETIME(UBoiPlayerCard, PlayerImageTexture);
	DOREPLIFETIME(UBoiPlayerCard, PlayerStatus);
}
