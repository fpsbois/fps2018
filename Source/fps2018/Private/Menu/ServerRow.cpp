// Fill out your copyright notice in the Description page of Project Settings.

#include "ServerRow.h"

#include "Components/Button.h"

#include "BoiMainMenu.h"

void UServerRow::Setup(class UBoiMainMenu* Parent, uint32 Index)
{
	this->Parent = Parent;
	this->Index = Index;

	ServerRowButton->OnClicked.AddDynamic(this, &UServerRow::OnClicked);
}

void UServerRow::OnClicked()
{
	Parent->SelectIndex(Index);
}