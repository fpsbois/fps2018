// Copyright © FPSBois 2018

#include "BoiNameWidget.h"

#include "Components/TextBlock.h"

void UBoiNameWidget::SetPlayerNameText(FString Text)
{
	PlayerNameText->Text = FText::FromString(Text);
}
