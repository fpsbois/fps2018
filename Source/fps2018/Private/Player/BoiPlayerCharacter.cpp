// Copyright FPSBois 2018

#include "BoiPlayerCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/WidgetComponent.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine.h"

#include "BoiCreateAClassSave.h"
#include "BoiGameInstance.h"
#include "BoiGameMode.h"
#include "BoiGrenade.h"
#include "BoiHealthComponent.h"
#include "BoiNameWidgetComponent.h"
#include "BoiPlayerController.h"
#include "BoiPlayerDetectComponent.h"
#include "BoiPlayerState.h"
#include "BoiWeapon.h"
#include "BoiWeaponDataAsset.h"
#include "BoiWeaponDrop.h"
#include "BoiWeaponLibrary.h"
#include "fps2018.h"


ABoiPlayerCharacter::ABoiPlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(GetCapsuleComponent());
	CameraComp->RelativeLocation = FVector(0, 0, BaseEyeHeight);
	CameraComp->bUsePawnControlRotation = true;

	HealthComp = CreateDefaultSubobject<UBoiHealthComponent>(TEXT("HealthComp"));

	NameWidgetComp = CreateDefaultSubobject<UBoiNameWidgetComponent>(TEXT("NameWidgetComp"));
	NameWidgetComp->SetupAttachment(RootComponent);

	PlayerDetectComp = CreateDefaultSubobject<UBoiPlayerDetectComponent>(TEXT("PlayerDetectComp"));
	
	FPPMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FPPMesh"));
	FPPMeshComp->SetupAttachment(CameraComp);
	FPPMeshComp->bOnlyOwnerSee = true;
	FPPMeshComp->bOwnerNoSee = false;
	FPPMeshComp->bCastDynamicShadow = false;
	FPPMeshComp->bReceivesDecals = false;
	FPPMeshComp->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	FPPMeshComp->PrimaryComponentTick.TickGroup = TG_PrePhysics;
	FPPMeshComp->SetCollisionObjectType(ECC_Pawn);
	FPPMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	FPPMeshComp->SetCollisionResponseToAllChannels(ECR_Ignore);

	GetMesh()->bOnlyOwnerSee = false;
	GetMesh()->bOwnerNoSee = true;
	GetMesh()->bReceivesDecals = false;
	GetMesh()->SetCollisionObjectType(ECC_Pawn);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;
}

void ABoiPlayerCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (Role == ROLE_Authority)
	{
		SpawnDefaultInventory();
	}

	GetCharacterMovement()->MaxWalkSpeed = MoveSpeed;
	GetCharacterMovement()->MaxWalkSpeedCrouched = CrouchSpeed;

	HealthComp->OnHealthDead.AddDynamic(this, &ABoiPlayerCharacter::OnDied);

	FOVStorage = CameraComp->FieldOfView;
	
	// set initial mesh visibility (3rd person view)
	UpdatePawnMeshes();

	// create material instance for setting team colors (3rd person view)
	for (int32 iMat = 0; iMat < GetMesh()->GetNumMaterials(); iMat++)
	{
		//MeshMIDs.Add(GetMesh()->CreateAndSetMaterialInstanceDynamic(iMat));
	}
}

void ABoiPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	
}

void ABoiPlayerCharacter::EndPlay(EEndPlayReason::Type EndplayReason)
{
	Super::EndPlay(EndplayReason);
}

void ABoiPlayerCharacter::UpdatePawnMeshes()
{
	bool const bFirstPerson = IsFirstPerson();

	FPPMeshComp->MeshComponentUpdateFlag = !bFirstPerson ? EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered : EMeshComponentUpdateFlag::AlwaysTickPoseAndRefreshBones;
	FPPMeshComp->SetOwnerNoSee(!bFirstPerson);

	GetMesh()->MeshComponentUpdateFlag = bFirstPerson ? EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered : EMeshComponentUpdateFlag::AlwaysTickPoseAndRefreshBones;
	GetMesh()->SetOwnerNoSee(bFirstPerson);
}

void ABoiPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("AimDownSight", IE_Pressed, this, &ABoiPlayerCharacter::BeginADS);
	PlayerInputComponent->BindAction("AimDownSight", IE_Released, this, &ABoiPlayerCharacter::EndADS);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ABoiPlayerCharacter::CrouchToggle);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ABoiPlayerCharacter::BeginFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ABoiPlayerCharacter::EndFire);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ABoiPlayerCharacter::Jump);

	PlayerInputComponent->BindAction("Prone", IE_Pressed, this, &ABoiPlayerCharacter::ProneToggle);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ABoiPlayerCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ABoiPlayerCharacter::StopSprinting);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ABoiPlayerCharacter::Interact);

	PlayerInputComponent->BindAxis("LookUp", this, &ABoiPlayerCharacter::LookUp);

	PlayerInputComponent->BindAxis("MoveForward", this, &ABoiPlayerCharacter::MoveForward);

	PlayerInputComponent->BindAxis("MoveRight", this, &ABoiPlayerCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &ABoiPlayerCharacter::Turn);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ABoiPlayerCharacter::Reload);

	PlayerInputComponent->BindAction("Grenade", IE_Pressed, this, &ABoiPlayerCharacter::PullGrenadeFuse);
	PlayerInputComponent->BindAction("Grenade", IE_Released, this, &ABoiPlayerCharacter::ThrowGrenade);

	PlayerInputComponent->BindAction("EquipPrimary", IE_Pressed, this, &ABoiPlayerCharacter::EquipPrimary);
	PlayerInputComponent->BindAction("EquipSecondary", IE_Pressed, this, &ABoiPlayerCharacter::EquipSecondary);
}

void ABoiPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsSprinting())
	{
		AddMovementInput(GetActorForwardVector());
	}

	float TargetFOV = FOVStorage;
	float ADSSpeed = DefaultADSSpeed;
	if (CurrentWeapon && IsADS())
	{
		TargetFOV = CurrentWeapon->GetADSFOV();
		ADSSpeed = CurrentWeapon->GetADSSpeed();
	}

	CameraComp->SetFieldOfView(FMath::FInterpTo(CameraComp->FieldOfView, TargetFOV, DeltaTime, ADSSpeed));
	

	//if (GEngine && CurrentWeapon) GEngine->AddOnScreenDebugMessage(123, 1.5, FColor::Red, "IsFiring: " + CurrentWeapon->IsFiring());
}

// Only called by the server
void ABoiPlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	/// Setup info from PlayerState
	ABoiPlayerState* State = Cast<ABoiPlayerState>(NewController->PlayerState);
	if (State)
	{
		HealthComp->SetTeam(State->GetTeam());
		NameWidgetComp->SetName(State->GetPlayerName());
	}

	/// Begin scanning for other players
	PlayerDetectComp->Client_ActivateDetection();
}

void ABoiPlayerCharacter::PawnClientRestart()
{
	Super::PawnClientRestart();

	// switch mesh to 1st person view
	UpdatePawnMeshes();

	// reattach weapon if needed
	SetCurrentWeapon(CurrentWeapon);
}

bool ABoiPlayerCharacter::IsCrouching() const
{
	return GetMovementComponent()->IsCrouching();
}

bool ABoiPlayerCharacter::IsFalling() const
{
	return GetMovementComponent()->IsFalling();
}

bool ABoiPlayerCharacter::IsReloading() const
{
	return CurrentWeapon && CurrentWeapon->IsReloading();
}

USkeletalMeshComponent* ABoiPlayerCharacter::GetSpecifcPawnMesh(bool WantFirstPerson) const
{
	return WantFirstPerson == true ? FPPMeshComp : GetMesh();
}

USkeletalMeshComponent* ABoiPlayerCharacter::GetPawnMesh() const
{
	return IsFirstPerson() ? FPPMeshComp : GetMesh();
}

bool ABoiPlayerCharacter::IsFirstPerson() const
{
	return Controller && Controller->IsLocalPlayerController();
}

void ABoiPlayerCharacter::SetWeaponDrop(ABoiWeaponDrop* NewWeaponDrop)
{
	WeaponDrop = NewWeaponDrop;

	if (Role == ROLE_Authority)
	{
		OnRep_WeaponDrop();
	}
}

void ABoiPlayerCharacter::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}

void ABoiPlayerCharacter::MoveRight(float Value)
{
	if (!bSprinting)
	{
		AddMovementInput(GetActorRightVector() * Value);
	}
	else
	{
		AddMovementInput(GetActorRightVector() * Value * .5f);
	}
}

void ABoiPlayerCharacter::Sprint()
{
	if (IsADS())
	{
		return;
	}

	bSprinting = true;

	GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;

	if (IsReloading())
	{
		CurrentWeapon->StopReload();
	}

	UnCrouch();
	EndFire();

	if (Role == ROLE_AutonomousProxy)
	{
		ServerStartSprinting();
	}
}

void ABoiPlayerCharacter::ServerStartSprinting_Implementation()
{
	Sprint();
}

bool ABoiPlayerCharacter::ServerStartSprinting_Validate()
{
	return true;
}

void ABoiPlayerCharacter::StopSprinting()
{
	bSprinting = false;

	GetCharacterMovement()->MaxWalkSpeed = MoveSpeed;

	if (Role == ROLE_AutonomousProxy)
	{
		ServerStopSprinting();
	}
}

void ABoiPlayerCharacter::ServerStopSprinting_Implementation()
{
	StopSprinting();
}

bool ABoiPlayerCharacter::ServerStopSprinting_Validate()
{
	return true;
}

void ABoiPlayerCharacter::LookUp(float Value)
{
	AddControllerPitchInput(Value);

	if (bFiring && CurrentWeapon)
	{
		CurrentWeapon->ControlRecoil(Value, 0);
	}

	Pitch = GetControlRotation().Pitch;

	if (IsLocallyControlled() && Role == ROLE_AutonomousProxy)
	{
		ServerLookUp(Pitch);
	}
}

void ABoiPlayerCharacter::ServerLookUp_Implementation(float Value)
{
	Pitch = Value;
}

bool ABoiPlayerCharacter::ServerLookUp_Validate(float Value)
{
	return true;
}

void ABoiPlayerCharacter::Turn(float Value)
{
	AddControllerYawInput(Value);

	if (bFiring && CurrentWeapon)
	{
		CurrentWeapon->ControlRecoil(0, Value);
	}
}


void ABoiPlayerCharacter::CrouchToggle()
{
	if (!bIsCrouched)
	{
		Crouch();
	}
	else
	{
		UnCrouch();
	}
}

void ABoiPlayerCharacter::Interact()
{
	if (IsLocallyControlled() && WeaponDrop)
	{
		Server_InteractWithWeaponDrop();
	}
}


void ABoiPlayerCharacter::Server_InteractWithWeaponDrop_Implementation()
{
	if (WeaponDrop)
	{
		CurrentWeapon->OnLeaveInventory();
		ABoiWeapon* NewWeapon = WeaponDrop->SwitchWeapon(CurrentWeapon);
		AddWeapon(NewWeapon);
		EquipWeapon(NewWeapon);
	}
}

bool ABoiPlayerCharacter::Server_InteractWithWeaponDrop_Validate()
{
	return true;
}

void ABoiPlayerCharacter::BeginADS()
{
	if (IsReloading())
	{
		return;
	}

	if (IsSprinting())
	{
		StopSprinting();
	}

	if (Role == ROLE_AutonomousProxy)
	{
		ServerBeginADS();
	}

	bAimDownSights = true;
	UpdateMovementSpeed();

	if (!CurrentWeapon) { return; }

}

void ABoiPlayerCharacter::ServerBeginADS_Implementation()
{
	BeginADS();
}

bool ABoiPlayerCharacter::ServerBeginADS_Validate()
{
	return true;
}

void ABoiPlayerCharacter::EndADS()
{
	bAimDownSights = false;
	UpdateMovementSpeed();

	if (Role == ROLE_AutonomousProxy)
	{
		ServerEndADS();
	}
}

void ABoiPlayerCharacter::ServerEndADS_Implementation()
{
	EndADS();
}

bool ABoiPlayerCharacter::ServerEndADS_Validate()
{
	return true;
}

void ABoiPlayerCharacter::ProneToggle()
{
	if (Role == ROLE_AutonomousProxy)
	{
		ServerProneToggle();
	}

	if (bIsCrouched)
	{
		UnCrouch();
	}

	bProne = !bProne;

	if (bProne)
	{
		GetCharacterMovement()->CrouchedHalfHeight = ProneHalfHeight;
		GetCharacterMovement()->MaxWalkSpeedCrouched = ProneSpeed;
		Crouch();
	}
	else
	{
		GetCharacterMovement()->CrouchedHalfHeight = CrouchedHalfHeight;
		GetCharacterMovement()->MaxWalkSpeedCrouched = CrouchSpeed;
		UnCrouch();
	}
}

void ABoiPlayerCharacter::ServerProneToggle_Implementation()
{
	ProneToggle();
}

bool ABoiPlayerCharacter::ServerProneToggle_Validate()
{
	return true;
}

void ABoiPlayerCharacter::BeginFire()
{
	if (IsReloading())
	{
		return;
	}

	if (IsSprinting())
	{
		StopSprinting();
	}

	if (CurrentWeapon && !CurrentGrenade)
	{
		CurrentWeapon->StartFire();
		bFiring = true;
	}
}

void ABoiPlayerCharacter::EndFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
		bFiring = false;
	}
}

void ABoiPlayerCharacter::PullGrenadeFuse()
{
	if (bFiring || IsReloading())
	{
		return;
	}

	if (DefaultGrenade && GrenadeCount > 0)
	{
		if (HasAuthority())
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			FVector Loc;
			FRotator Rot;
			Controller->GetActorEyesViewPoint(Loc, Rot);
			CurrentGrenade = GetWorld()->SpawnActor<ABoiGrenade>(DefaultGrenade, Loc + CameraComp->GetForwardVector(), Rot, SpawnParams);
			CurrentGrenade->GetMesh()->SetSimulatePhysics(false);
			CurrentGrenade->GetMesh()->SetEnableGravity(false);
			CurrentGrenade->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocketName);
			CurrentGrenade->SetOwner(this);
			CurrentGrenade->PullFuse();
			CurrentGrenade->GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

			GrenadeCount--;
		}
		else
		{
			Server_PullGrenadeFuse();
		}
	}
}

void ABoiPlayerCharacter::Server_PullGrenadeFuse_Implementation()
{
	PullGrenadeFuse();
}

bool ABoiPlayerCharacter::Server_PullGrenadeFuse_Validate()
{
	return true;
}

void ABoiPlayerCharacter::ThrowGrenade()
{
	if (CurrentGrenade)
	{
		if (Role == ROLE_Authority) 
		{
			CurrentGrenade->Throw(CameraComp->GetForwardVector());
		}
		else
		{
			Server_ThrowGrenade();
		}
	}
	CurrentGrenade = nullptr;
}

void ABoiPlayerCharacter::Server_ThrowGrenade_Implementation()
{
	ThrowGrenade();
}

bool ABoiPlayerCharacter::Server_ThrowGrenade_Validate()
{
	return true;
}

void ABoiPlayerCharacter::Reload()
{
	if (CurrentWeapon && !CurrentGrenade)
	{
		CurrentWeapon->StartReload();
	}
}

void ABoiPlayerCharacter::Jump()
{
	if (!bCanReloadWhileJumping && IsReloading())
	{
		CurrentWeapon->StopReload();
	}

	if (IsCrouching())
	{
		UnCrouch();
	}

	Super::Jump();
}

void ABoiPlayerCharacter::UpdateMovementSpeed()
{
	if (IsCrouching() && !IsProne())
	{
		if (IsADS())
		{
			GetCharacterMovement()->MaxWalkSpeedCrouched = CrouchSpeedADS;
		}
		else
		{
			GetCharacterMovement()->MaxWalkSpeedCrouched = CrouchSpeed;
		}
	}
	else if (IsProne())
	{
		if (IsADS())
		{
			// TODO: ADS and Prone? Is movement disabled? 
		}
		else
		{
			GetCharacterMovement()->MaxWalkSpeedCrouched = ProneSpeed;
		}
	}
	else if (IsSprinting())
	{
		GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	}
	else if (IsADS())
	{
		GetCharacterMovement()->MaxWalkSpeed = ADSMoveSpeed;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = MoveSpeed;
	}
}



void ABoiPlayerCharacter::SpawnDefaultInventory()
{
	if (Role < ROLE_Authority)
	{
		return;
	}

	// TODO: The string should be in fps2018.h I think?
	// We should display a menu to pick a class
	UBoiCreateAClassSave* SG1 = Cast<UBoiCreateAClassSave>(UGameplayStatics::LoadGameFromSlot("CustomClass1", 0));

	if (!SG1) {
		SG1 = Cast<UBoiCreateAClassSave>(UGameplayStatics::CreateSaveGameObject(CreateAClassSaveClass));
		SG1->PrimaryWeaponIndex = 0;
		SG1->SecondaryWeaponIndex = 0;
	}

	if (!GetGameInstance()) { return; }

	UBoiWeaponLibrary* WeaponLibrary = Cast<UBoiGameInstance>(GetGameInstance())->GetWeaponLibrary();

	if (WeaponLibrary->PrimaryWeapons[SG1->PrimaryWeaponIndex])
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ABoiWeapon* NewWeapon = GetWorld()->SpawnActor<ABoiWeapon>(WeaponLibrary->PrimaryWeapons[SG1->PrimaryWeaponIndex]->Blueprint, SpawnInfo);
		AddWeapon(NewWeapon);
	}
	if (WeaponLibrary->SecondaryWeapons[SG1->SecondaryWeaponIndex])
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ABoiWeapon* NewWeapon = GetWorld()->SpawnActor<ABoiWeapon>(WeaponLibrary->SecondaryWeapons[SG1->SecondaryWeaponIndex]->Blueprint, SpawnInfo);
		AddWeapon(NewWeapon);
	}

	// equip first weapon in inventory
	if (Inventory.Num() > 0)
	{
		EquipWeapon(Inventory[0]);
	}
}

void ABoiPlayerCharacter::EquipWeapon(ABoiWeapon* Weapon)
{
	if (Weapon)
	{
		if (Role == ROLE_Authority)
		{
			SetCurrentWeapon(Weapon, CurrentWeapon);
		}
		else if (IsLocallyControlled())
		{
			ServerEquipWeapon(Weapon);
		}
	}
}

void ABoiPlayerCharacter::ServerEquipWeapon_Implementation(ABoiWeapon* NewWeapon)
{
	EquipWeapon(NewWeapon);
}

bool ABoiPlayerCharacter::ServerEquipWeapon_Validate(ABoiWeapon* NewWeapon)
{
	return true;
}

void ABoiPlayerCharacter::SetCurrentWeapon(ABoiWeapon* NewWeapon, ABoiWeapon* LastWeapon /*= nullptr*/)
{
	ABoiWeapon* LocalLastWeapon = nullptr;

	if (LastWeapon != nullptr)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentWeapon)
	{
		LocalLastWeapon = CurrentWeapon;
	}

	if (LocalLastWeapon)
	{
		LocalLastWeapon->Unequip();
	}

	CurrentWeapon = NewWeapon;

	// equip new one
	if (NewWeapon)
	{
		NewWeapon->SetOwningPawn(this);	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!
		NewWeapon->Equip(LastWeapon);
	}
}

void ABoiPlayerCharacter::AddWeapon(ABoiWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		Weapon->OnEnterInventory(this);
		Inventory.AddUnique(Weapon);
	}
}

void ABoiPlayerCharacter::OnRep_WeaponDrop()
{
	if (WeaponDrop && IsLocallyControlled())
	{
		// TODO: Inform UI that we can pick a weapon up
	}
}

void ABoiPlayerCharacter::OnDied(const UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (bDied || !HealthComp->IsDead()) 
	{
		return;
	}

	bDied = true;

	// Tell the GameMode that an actor died
	ABoiGameMode* GM = Cast<ABoiGameMode>(GetWorld()->GetAuthGameMode());
	if (GM)
	{
		GM->OnActorKilled.Broadcast(this, GetController(), DamageCauser, InstigatedBy);
	}
	
	// if we are holding a grenade, drop it
	if (CurrentGrenade)
	{
		CurrentGrenade->GetMesh()->SetSimulatePhysics(true);
		CurrentGrenade->GetMesh()->SetEnableGravity(true);
		CurrentGrenade->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		CurrentGrenade->GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	}

	// Spawn a weapon drop
	/*if (WeaponDropClass)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ABoiWeaponDrop* WeapDrop = GetWorld()->SpawnActor<ABoiWeaponDrop>(WeaponDropClass, GetActorLocation() + FVector::UpVector * 100, FRotator::ZeroRotator, SpawnParams);
		if (WeapDrop)
		{
			CurrentWeapon->OnLeaveInventory();
			WeapDrop->SetWeapon(this, CurrentWeapon);
			CurrentWeapon = nullptr;
		}
		else
		{
			CurrentWeapon->Destroy();
		}
	}
	else
	{
		CurrentWeapon->Destroy();
	}*/CurrentWeapon->Destroy();

	// Get ready for destruction 
	GetMovementComponent()->StopMovementImmediately();
	DetachFromControllerPendingDestroy();
	SetLifeSpan(4.0f);
	OnRep_bDied();
}

void ABoiPlayerCharacter::OnRep_bDied()
{
	if (bDied)
	{
		GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);
		GetCapsuleComponent()->SetEnableGravity(false);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

void ABoiPlayerCharacter::OnRep_CurrentWeapon(ABoiWeapon* LastWeapon)
{
	SetCurrentWeapon(CurrentWeapon, LastWeapon);
}

void ABoiPlayerCharacter::EquipPrimary()
{
	if (Inventory[0] && (CurrentWeapon == NULL || CurrentWeapon->GetCurrentState() != EWeaponState::Equipping))
	{
		ABoiWeapon* NextWeapon = Inventory[0];
		EquipWeapon(NextWeapon);
	}
}

void ABoiPlayerCharacter::EquipSecondary()
{
	if (Inventory[1] && (CurrentWeapon == NULL || CurrentWeapon->GetCurrentState() != EWeaponState::Equipping))
	{
		ABoiWeapon* NextWeapon = Inventory[1];
		EquipWeapon(NextWeapon);
	}
}

void ABoiPlayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// only to local owner: weapon change requests are locally instigated, other clients don't need it
	DOREPLIFETIME_CONDITION(ABoiPlayerCharacter, Inventory, COND_OwnerOnly);

	DOREPLIFETIME(ABoiPlayerCharacter, bDied);
	DOREPLIFETIME(ABoiPlayerCharacter, bProne);

	DOREPLIFETIME(ABoiPlayerCharacter, CurrentWeapon)

	DOREPLIFETIME_CONDITION(ABoiPlayerCharacter, WeaponDrop, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(ABoiPlayerCharacter, Pitch, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ABoiPlayerCharacter, bAimDownSights, COND_SkipOwner);

	DOREPLIFETIME_CONDITION(ABoiPlayerCharacter, CurrentGrenade, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABoiPlayerCharacter, GrenadeCount, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABoiPlayerCharacter, DefaultGrenade, COND_OwnerOnly);
}