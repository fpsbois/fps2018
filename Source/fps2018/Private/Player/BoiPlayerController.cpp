// Copyright FPSBois 2018

#include "BoiPlayerController.h"

#include "Blueprint/UserWidget.h"
#include "Net/UnrealNetwork.h"
#include "UObject/UObjectIterator.h"

#include "BoiCrosshair.h"
#include "BoiGameInstance.h"
#include "BoiPlayerDetectComponent.h"

void ABoiPlayerController::SetReticleColor(EDetectionType DetectionType)
{
	if (!Crosshair) { return; }
	Crosshair->SetColorFromDetectionType(DetectionType);
}

void ABoiPlayerController::OnPlayerKilled(FString Killer, FString Victim)
{

}

void ABoiPlayerController::Possess(APawn* aPawn)
{
	Super::Possess(aPawn);
	OnPossessed(aPawn);
}

void ABoiPlayerController::UnPossess()
{
	Super::UnPossess();
	OnUnPossessed();
}

void ABoiPlayerController::Client_ClearHUDWidgets_Implementation()
{
	for (TObjectIterator<UUserWidget> Itr; Itr; ++Itr) {
		UUserWidget* LiveWidget = *Itr;

		/* If the Widget has no World, Ignore it (It's probably in the Content Browser!) */ if (!LiveWidget->GetWorld()) { continue; }
		else { LiveWidget->RemoveFromParent(); }
	}
}

void ABoiPlayerController::BeginPlay()
{
	Super::BeginPlay();

	//if (IsLocalPlayerController())
	//{
	//	/* Object Iterator for All User Widgets! */
	//	for (TObjectIterator<UUserWidget> Itr; Itr; ++Itr)
	//	{
	//		UUserWidget* LiveWidget = *Itr;

	//		/* If the Widget has no World, Ignore it (It's probably in the Content Browser!) */
	//		if (!LiveWidget->GetWorld())
	//		{
	//			continue;
	//		}
	//		else
	//		{
	//			LiveWidget->RemoveFromParent();
	//		}
	//	}
	//}
	//} 

	if (IsLocalPlayerController())
	{
		if (ensure(CrosshairClass))
		{
			Crosshair = CreateWidget<UBoiCrosshair>(this, CrosshairClass);
			Crosshair->AddToViewport();
		}
	}
}

void ABoiPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("OpenMenu", IE_Released, this, &ABoiPlayerController::OpenMenu);
}

void ABoiPlayerController::OpenMenu()
{
	UBoiGameInstance* GI = Cast<UBoiGameInstance>(GetGameInstance());

	if (GI)
	{
		GI->ShowInGameMenu();
	}
}

void ABoiPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABoiPlayerController, PlayerSettings);
}