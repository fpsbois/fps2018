// Copyright © FPSBois 2018

#include "BoiPlayerSaveGame.h"

#include "Net/UnrealNetwork.h"


void UBoiPlayerSaveGame::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBoiPlayerSaveGame, PlayerInfo);
}
