// Copyright © FPSBois 2018

#include "BoiPlayerState.h"

void ABoiPlayerState::SetTeam(uint8 TeamNumber)
{
	Team = TeamNumber;
}

void ABoiPlayerState::AddScore(float ScoreDelta)
{
	Score += ScoreDelta;
}

void ABoiPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABoiPlayerState, Team);
}