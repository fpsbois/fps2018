// Copyright FPSBois 2018

#include "BoiGrenade.h"

#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "TimerManager.h"
#include "CoreMinimal.h"
#include "Engine/World.h"

#include "fps2018.h"

// Sets default values
ABoiGrenade::ABoiGrenade()
{
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetSimulatePhysics(true);
	RootComponent = MeshComp;

	RadialForceComp = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComp"));

	ProjectileComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileComp->bShouldBounce = true;

	FuseTime = 3.0f;
	DamageInnerRadius = 100.0f;
	DamageOuterRadius = 500.0f;
	DamageFalloff = 2.0f;
	ThrowSpeed = 2000.0f;
	BaseDamage = 50.0f;
	MinDamage = 20.0f;

	SetReplicates(true);
	SetReplicateMovement(true);
}

void ABoiGrenade::PullFuse()
{
	GetWorldTimerManager().SetTimer(TimerHandle_Fuse, this, &ABoiGrenade::OnFuseEnded, FuseTime, false);
}


void ABoiGrenade::PullFuse(float Time)
{
	GetWorldTimerManager().SetTimer(TimerHandle_Fuse, this, &ABoiGrenade::OnFuseEnded, Time, false);
}

void ABoiGrenade::Throw(FVector Direction)
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	ABoiGrenade* CloneGrenade = GetWorld()->SpawnActor<ABoiGrenade>(GetClass(), GetActorLocation(), Direction.Rotation(), SpawnParams);

	float TimeRemaining = GetWorldTimerManager().GetTimerRemaining(TimerHandle_Fuse);
	CloneGrenade->PullFuse(TimeRemaining);
	CloneGrenade->ProjectileComp->Activate();
	CloneGrenade->SetOwner(GetOwner());
	GetWorldTimerManager().ClearAllTimersForObject(this);
	Destroy();
}

void ABoiGrenade::OnFuseEnded()
{
	Client_PlayExplosionEffects();

	APawn* Pawn = Cast<APawn>(GetOwner());
	AController* Controller = nullptr;
	if (Pawn)
	{
		Controller = Pawn->GetController();
	}
	
	RadialForceComp->FireImpulse();
	TArray<AActor*> IgnoreActors;
	UGameplayStatics::ApplyRadialDamageWithFalloff(this, BaseDamage, MinDamage, GetActorLocation(), DamageInnerRadius, DamageOuterRadius, DamageFalloff, DamageType, IgnoreActors, this, Controller, COLLISION_GRENADE);

	Destroy();
}

void ABoiGrenade::Client_PlayExplosionEffects_Implementation()
{
	if (ExplosionEffects)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionEffects, GetActorLocation());
	}
	if (ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());
	}
}
