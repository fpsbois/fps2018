// Copyright FPSBois 2018

#include "BoiWeapon.h"

#include "DrawDebugHelpers.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "GameFramework/Character.h"
#include "Net/UnrealNetwork.h"

#include "BoiPlayerCharacter.h"
#include "BoiPlayerController.h"
#include "BoiHealthComponent.h"
#include "BoiWeaponDataAsset.h"

#include "fps2018.h"

static int32 DebugWeaponDrawing = 0;
FAutoConsoleVariableRef CVARDebugWeaponDrawing(
	TEXT("Boi.DebugWeapons"),
	DebugWeaponDrawing,
	TEXT("Draw Debug Lines for Weapons"),
	ECVF_Cheat
);

ABoiWeapon::ABoiWeapon()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("ROOT"));

	FPPMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FPPMeshComp"));
	FPPMeshComp->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	FPPMeshComp->bReceivesDecals = false;
	FPPMeshComp->bOnlyOwnerSee = true;
	FPPMeshComp->bOwnerNoSee = false;
	FPPMeshComp->CastShadow = false;
	FPPMeshComp->SetCollisionObjectType(ECC_WorldDynamic);
	FPPMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	FPPMeshComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	FPPMeshComp->SetupAttachment(RootComponent);

	TPPMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TPPMeshComp"));
	TPPMeshComp->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	TPPMeshComp->bReceivesDecals = false;
	TPPMeshComp->bOnlyOwnerSee = false;
	TPPMeshComp->bOwnerNoSee = true;
	TPPMeshComp->CastShadow = true;
	TPPMeshComp->SetCollisionObjectType(ECC_WorldDynamic);
	TPPMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	TPPMeshComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	TPPMeshComp->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	TPPMeshComp->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	TPPMeshComp->SetupAttachment(RootComponent);

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	bReplicates = true;
	bNetUseOwnerRelevancy = true;

	//NetUpdateFrequency = 66.0f;
	//MinNetUpdateFrequency = 33.0f;
}

void ABoiWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CurrentAmmoInWeapon = MaxAmmoInGun;
	CurrentAmmoInBackpack = MaxAmmoInBackpack;

	DetachMeshFromPawn();
}

void ABoiWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (!ensure(WeaponData))
	{
		UE_LOG(LogTemp, Warning, TEXT("Weapon Missing WeaponData"));
		return;
	}

	CurrentAmmoInWeapon = MaxAmmoInGun;

	TimeBetweenShots = 60 / RateOfFire;

}

void ABoiWeapon::Tick(float DeltaTime)
{
	RecoilReturn(DeltaTime);
	ShrinkSpread(DeltaTime);
}


void ABoiWeapon::Fire()
{
	AActor* MyOwner = GetOwner();

	if (!MyOwner) { return; }

	// Ammo Check
	if (CurrentAmmoInWeapon <= 0)
	{
		// TODO: Should this only play on locally controlled actors? - Christian Roy 7/30/2018
		// If other players will be able to hear this, then it should be a 3D sound.
		if (EmptyMagSound)
		{
			UGameplayStatics::PlaySound2D(this, EmptyMagSound);
		}
		return;
	}

	bFiring = true;

	if (Role < ROLE_Authority)
	{
		ServerFire();
	}
	else
	{
		// TODO: Why does this method exist? Should handle weapon FX all at the same time across network - Christian Roy 7/30/2018
		MulticastPlayFireSound();
		CurrentAmmoInWeapon--;

		FVector EyeLocation;
		FRotator EyeRotation;

		MyPawn->Controller->GetPlayerViewPoint(EyeLocation, EyeRotation);

		const float HalfRad = FMath::DegreesToRadians(CurrentSpread);
		const FVector TraceStart = EyeLocation;
		const FVector AimDir = EyeRotation.Vector();
		const FVector ShootDir = FMath::VRandCone(AimDir, HalfRad);
		FVector TraceDest = EyeLocation + ShootDir * RayScanRange;

		/// Debug drawing
		if (DebugWeaponDrawing > 0)
		{
			DrawDebugLine(GetWorld(), TraceStart, TraceDest, FColor::White, false, 1.0f, 0, .5f);
		}

		const FHitResult Hit = WeaponTrace(TraceStart, TraceDest);

		if (Hit.bBlockingHit)
		{
			AActor* HitActor = Hit.GetActor();

			HitScanTrace.TraceTo = Hit.ImpactPoint;
			HitScanTrace.ImpactNormal = Hit.ImpactNormal;
			HitScanTrace.SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());
			HitScanTrace.HitActor = HitActor;
			HitScanTrace.bHitActorWithHealth = false;

			/// Check for head shot or other vulnerable spots, phys mat set on phys skeleton
			float DistanceToHitActor = FVector::Dist(HitScanTrace.TraceTo, TraceStart);
			float ActualDamage = BaseDamage * DamageFalloffCurve->GetFloatValue(DistanceToHitActor);
			ActualDamage *= (HitScanTrace.SurfaceType == SURFACE_FLESHVULNERABLE ? HeadshotMultiplier : 1.0f);

			UGameplayStatics::ApplyPointDamage(HitActor, ActualDamage, ShootDir, Hit, MyOwner->GetInstigatorController(), this, DamageType);

			/// Shot Hit Markers
			if (HitActor) {
				auto OtherHealthComp = Cast<UBoiHealthComponent>(HitActor->GetComponentByClass(UBoiHealthComponent::StaticClass()));
				if (OtherHealthComp)
				{
					HitScanTrace.bHitActorWithHealth = true;
					HitScanTrace.bKilledActor = (OtherHealthComp->GetCurrentHealth() <= 0);
				}
			}
		}
		else
		{
			HitScanTrace.bHitActorWithHealth = false;
			HitScanTrace.bKilledActor = false;
			HitScanTrace.SurfaceType = SurfaceType_Default;
			HitScanTrace.ImpactNormal = FVector::ZeroVector;
			HitScanTrace.TraceTo = TraceDest;
			HitScanTrace.HitActor = nullptr;
		}

		OnRep_HitScanTrace();
	}

	LastFiredTime = GetWorld()->TimeSeconds;

	WidenSpreadOnFire();
	ApplyRecoil();

}

FHitResult ABoiWeapon::WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const
{
	// Perform trace to retrieve hit info
	FCollisionQueryParams QueryParams;
	AActor* MyOwner = GetOwner();

	if (MyOwner)
	{
		QueryParams.AddIgnoredActor(MyOwner);
	}

	QueryParams.AddIgnoredActor(this);
	QueryParams.bTraceComplex = true;
	QueryParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, COLLISION_WEAPON, QueryParams);

	return Hit;
}

void ABoiWeapon::AttachMeshToPawn()
{
	if (!MyPawn) { return; }

	// Remove and hide both first and third person meshes
	DetachMeshFromPawn();

	// For locally controller players we attach both weapons and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
	FName AttachPoint = MyPawn->GetWeaponAttachPoint();
	FName AttachPointFP = MyPawn->GetWeaponAttachPointFP();
	if (MyPawn->IsLocallyControlled())
	{
		USkeletalMeshComponent* PawnMesh1p = MyPawn->GetSpecifcPawnMesh(true);
		USkeletalMeshComponent* PawnMesh3p = MyPawn->GetSpecifcPawnMesh(false);
		FPPMeshComp->SetHiddenInGame(false);
		TPPMeshComp->SetHiddenInGame(false);
		RootComponent->AttachToComponent(PawnMesh3p, FAttachmentTransformRules::KeepRelativeTransform);
		FPPMeshComp->AttachToComponent(PawnMesh1p, FAttachmentTransformRules::KeepRelativeTransform, AttachPointFP);
		TPPMeshComp->AttachToComponent(PawnMesh3p, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint);


		FPPMeshComp->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::AlwaysTickPoseAndRefreshBones;
	}
	else
	{
		USkeletalMeshComponent* UseWeaponMesh = GetWeaponMesh();
		USkeletalMeshComponent* UsePawnMesh = MyPawn->GetPawnMesh();
		RootComponent->AttachToComponent(UsePawnMesh, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint);
		UseWeaponMesh->AttachToComponent(UsePawnMesh, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint);
		UseWeaponMesh->SetHiddenInGame(false);


		FPPMeshComp->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	}
}

void ABoiWeapon::DetachMeshFromPawn()
{
	RootComponent->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);

	FPPMeshComp->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	FPPMeshComp->SetHiddenInGame(true);

	TPPMeshComp->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	TPPMeshComp->SetHiddenInGame(true);
}

USkeletalMeshComponent* ABoiWeapon::GetWeaponMesh() const
{
	return (MyPawn != NULL && MyPawn->IsFirstPerson()) ? FPPMeshComp : TPPMeshComp;
}

void ABoiWeapon::OnRep_HitScanTrace()
{
	PlayFireEffects(HitScanTrace.TraceTo);
	PlayImpactEffects(HitScanTrace.SurfaceType, HitScanTrace.TraceTo, HitScanTrace.ImpactNormal);
	
	if (!HitScanTrace.HitActor) { return; }

	auto OtherHealthComp = Cast<UBoiHealthComponent>(HitScanTrace.HitActor->GetComponentByClass(UBoiHealthComponent::StaticClass()));

	if (!OtherHealthComp) { return; }

	if (MyPawn)
	{
		ABoiPlayerController* MyPC = Cast<ABoiPlayerController>(MyPawn->GetController());
		if (MyPC)
		{
			MyPC->OnShotActorWithHealth(OtherHealthComp->IsDead());
		}
	}
}

bool ABoiWeapon::IsAttachedToPawn() const
{
	return bEquipped || bEquipping;
}

void ABoiWeapon::SetOwningPawn(ABoiPlayerCharacter* NewOwner)
{
	if (MyPawn != NewOwner)
	{
		Instigator = NewOwner;
		MyPawn = NewOwner;
		// net owner for RPC calls
		SetOwner(NewOwner);
	}
}

void ABoiWeapon::Equip(const ABoiWeapon* LastWeapon)
{
	AttachMeshToPawn();

	bEquipping = true;
	DetermineWeaponState();

	// Only play animation if last weapon is valid
	if (LastWeapon)
	{
		EquipStartTime = GetWorld()->GetTimeSeconds();
		GetWorldTimerManager().SetTimer(TimerHandle_OnEquipFinished, this, &ABoiWeapon::OnEquipFinished, EquipTime, false);
	}
	else
	{
		OnEquipFinished();
	}

	// TODO: Play reload sound on local controller only?
	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		//PlayWeaponSound(EquipSound);
	}
}

void ABoiWeapon::OnEquipFinished()
{
	AttachMeshToPawn();

	bEquipped = true;
	bEquipping = false;

	// Determine the state so that the can reload checks will work
	DetermineWeaponState();

	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		// try to reload empty clip
		if (CurrentAmmoInWeapon <= 0 && CanReload())
		{
			StartReload();
		}
	}
}

void ABoiWeapon::Unequip()
{
	DetachMeshFromPawn();
	bEquipped = false;
	StopFire();

	if (CurrentState == EWeaponState::Reloading)
	{
		StopReload();
	}

	if (bEquipping)
	{
		bEquipping = false;
		GetWorldTimerManager().ClearTimer(TimerHandle_OnEquipFinished);
	}

	DetermineWeaponState();
}

void ABoiWeapon::OnEnterInventory(ABoiPlayerCharacter* NewOwner)
{
	SetOwningPawn(NewOwner);
}

void ABoiWeapon::OnLeaveInventory()
{
	if (Role == ROLE_Authority)
	{
		SetOwningPawn(NULL);
	}

	if (IsAttachedToPawn())
	{
		Unequip();
	}
}

void ABoiWeapon::ServerFire_Implementation()
{
	Fire();
}

bool ABoiWeapon::ServerFire_Validate()
{
	return true;
}

bool ABoiWeapon::CanFire() const
{
	// bool bCanFire = MyPawn && MyPawn->CanFire();
	bool bStateOKToFire = (CurrentState == EWeaponState::Idle || CurrentState == EWeaponState::Firing);
	return bStateOKToFire && !bReloading;
}

void ABoiWeapon::StartFire()
{
	float FirstDelay = FMath::Max(LastFiredTime + TimeBetweenShots - GetWorld()->TimeSeconds, 0.0f);
	GetWorldTimerManager().SetTimer(TimerHandle_TimeBetweenShots, this, &ABoiWeapon::Fire, TimeBetweenShots, true, FirstDelay);

	RecoilOffset = FVector::ZeroVector;
	RecoilControlOffset = FVector::ZeroVector;
	bRecoilReturn = false;
}

void ABoiWeapon::StopFire()
{
	if (bFiring)
	{
		RecoilOffset.Y -= RecoilControlOffset.Y;
		RecoilOffset.Z -= RecoilControlOffset.Z;
		bRecoilReturn = true;
	}
	GetWorldTimerManager().ClearTimer(TimerHandle_TimeBetweenShots);
	bFiring = false;
}

void ABoiWeapon::PlayFireEffects(FVector TraceEnd)
{
	if (MuzzleFX)
	{
		USkeletalMeshComponent* UseWeaponMesh = GetWeaponMesh();
		if (!bLoopedMuzzleFX || MuzzlePSC == nullptr)
		{
			// Split screen requires we create 2 effects. One that we see and one that the other player sees.
			if ((MyPawn != nullptr) && (MyPawn->IsLocallyControlled() == true))
			{
				AController* PlayerCon = MyPawn->GetController();
				if (PlayerCon != nullptr)
				{
					FPPMeshComp->GetSocketLocation(MuzzleSocketName);
					MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, FPPMeshComp, MuzzleSocketName);
					MuzzlePSC->bOwnerNoSee = false;
					MuzzlePSC->bOnlyOwnerSee = true;

					TPPMeshComp->GetSocketLocation(MuzzleSocketName);
					MuzzlePSCSecondary = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, TPPMeshComp, MuzzleSocketName);
					MuzzlePSCSecondary->bOwnerNoSee = true;
					MuzzlePSCSecondary->bOnlyOwnerSee = false;
				}
			}
			else
			{
				MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, UseWeaponMesh, MuzzleSocketName);
			}
		}
	}

	if (TracerFX)
	{
		FVector MuzzleLocation = GetWeaponMesh()->GetSocketLocation(MuzzleSocketName);
		UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerFX, MuzzleLocation);
		if (TracerComp)
		{
			TracerComp->SetVectorParameter(TracerTargetName, TraceEnd);
		}
	}

	if (ShellFX)
	{
		FVector ShellEjectLocation = GetWeaponMesh()->GetSocketLocation(ShellEjectSocketName);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ShellFX, ShellEjectLocation);
	}

	APawn* MyOwner = Cast<APawn>(GetOwner());
	if (MyOwner)
	{
		APlayerController* PC = Cast<APlayerController>(MyOwner->GetController());
		if (PC)
		{
			PC->ClientPlayCameraShake(FireCamShake);
		}
	}
}

void ABoiWeapon::PlayImpactEffects(EPhysicalSurface SurfaceType, FVector ImpactPoint, FVector ImpactNormal)
{
	UParticleSystem* SelectedFX = nullptr;
	switch (SurfaceType)
	{
	case SURFACE_FLESHDEFAULT:
	case SURFACE_FLESHVULNERABLE:
		SelectedFX = FleshImpactFX;
		break;
	default:
		UDecalComponent * Decal = UGameplayStatics::SpawnDecalAtLocation(this, BulletHoleMaterial, FVector(20, 20, 10), ImpactPoint, (-ImpactNormal).Rotation(), 90.0f);
		SelectedFX = DefaultImpactFX;
		break;
	}

	if (SelectedFX)
	{
		FVector MuzzleLocation = FPPMeshComp->GetSocketLocation(MuzzleSocketName);
		FVector ShotDirection = ImpactPoint - MuzzleLocation;
		ShotDirection.Normalize();
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedFX, ImpactPoint, ShotDirection.Rotation());
	}

}

void ABoiWeapon::ApplyRecoil()
{
	float PitchRecoil = -FMath::RandRange(MinVertRecoil, MaxVertRecoil);
	float YawRecoil = FMath::RandRange(MinHorzRecoil, MaxHorzRecoil);

	ABoiPlayerCharacter* MyOwner = Cast<ABoiPlayerCharacter>(GetOwner());

	if (MyOwner)
	{
		if (MyOwner->IsADS())
		{
			PitchRecoil *= ADSRecoilMultiplier;
			YawRecoil *= ADSRecoilMultiplier;
		}

		if (MyOwner->IsProne())
		{
			PitchRecoil *= ProneRecoilMultiplier;
			YawRecoil *= ProneRecoilMultiplier;
		}
		else if (MyOwner->IsCrouching())
		{
			PitchRecoil *= CrouchRecoilMultiplier;
			YawRecoil *= CrouchRecoilMultiplier;
		}
	}

	MyOwner->AddControllerPitchInput(PitchRecoil);
	MyOwner->AddControllerYawInput(YawRecoil);

	RecoilOffset += FVector(0, PitchRecoil, YawRecoil);
}

void ABoiWeapon::ControlRecoil(float Pitch, float Yaw)
{
	RecoilControlOffset.Y -= Pitch;
	RecoilControlOffset.Z += Yaw;

	RecoilControlOffset.Z = FMath::Clamp(RecoilControlOffset.Z, -RecoilOffset.Z, RecoilOffset.Z);
}

FString ABoiWeapon::GetWeaponName() const
{
	if (!WeaponData) { return ""; }

	return WeaponData->Name;
}

void ABoiWeapon::RecoilReturn(float DeltaTime)
{
	float RecoilReturnSpeed = 2.0f;

	ACharacter* MyOwner = Cast<ACharacter>(GetOwner());
	if (bRecoilReturn && MyOwner)
	{
		float DeltaY = -RecoilOffset.Y * RecoilReturnSpeed * DeltaTime;
		float DeltaZ = -RecoilOffset.Z * RecoilReturnSpeed * DeltaTime;

		MyOwner->AddControllerPitchInput(DeltaY);
		MyOwner->AddControllerYawInput(DeltaZ);

		RecoilOffset += FVector(0, DeltaY, DeltaZ);

		if (RecoilOffset.Equals(FVector::ZeroVector))
		{
			bRecoilReturn = false;
			RecoilControlOffset = FVector::ZeroVector;
		}
	}
}

void ABoiWeapon::DetermineWeaponState()
{
	EWeaponState NewState = EWeaponState::Idle;

	if (bEquipped)
	{
		if (bReloading)
		{
			if (CanReload())
			{
				NewState = EWeaponState::Reloading;
			}
			else
			{
				NewState = CurrentState;
			}
		}
		else if (!bReloading && bFiring && CanFire())
		{
			NewState = EWeaponState::Firing;
		}
	}
	else if (bEquipping)
	{
		NewState = EWeaponState::Equipping;
	}

	SetWeaponState(NewState);
}

void ABoiWeapon::SetWeaponState(EWeaponState NewState)
{
	const EWeaponState PrevState = CurrentState;

	CurrentState = NewState;
}

void ABoiWeapon::ShrinkSpread(float DeltaTime)
{
	float SpreadDelta = SpreadDecreaseRate * DeltaTime;
	CurrentSpread -= SpreadDelta;
	CurrentSpread = FMath::ClampAngle(CurrentSpread, MinSpread, MaxSpread);
}

void ABoiWeapon::WidenSpreadOnFire()
{
	CurrentSpread += SpreadAngleIncreaseOnShotFired;
}

void ABoiWeapon::MulticastPlayFireSound_Implementation()
{
	FVector MuzzleLocation = FPPMeshComp->GetSocketLocation(MuzzleSocketName);

	if (FireSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, MuzzleLocation);
	}
}



bool ABoiWeapon::CanReload() const
{
	// TODO: Check if pawn is able to reload
	//bool bPawnCanReload = (!MyPawn || MyPawn->CanReload());
	bool bGunNotFull = CurrentAmmoInWeapon < MaxAmmoInGun;
	bool bHaveAmmo = CurrentAmmoInBackpack - (MaxAmmoInGun - CurrentAmmoInWeapon) > 0;
	bool bStateOKToReload = (CurrentState == EWeaponState::Idle) || (CurrentState == EWeaponState::Firing);

	return bGunNotFull && bHaveAmmo && bStateOKToReload;
}

void ABoiWeapon::StartReload(bool bFromReplication /*= false*/)
{

	if (!bFromReplication && Role < ROLE_Authority)
	{
		ServerStartReload();
	}

	if (bFromReplication || CanReload())
	{
		bReloading = true;
		DetermineWeaponState();

		// Timer for ammo
		if (Role == ROLE_Authority)
		{
			GetWorldTimerManager().SetTimer(TimerHandle_Reload, this, &ABoiWeapon::Reload, ReloadTime, false);
		}

		// Timer for state
		GetWorldTimerManager().SetTimer(TimerHandle_StopReload, this, &ABoiWeapon::StopReload, ReloadTime + .01f, false);

		// TODO: Play reload sound on local controller only?
		if (MyPawn && MyPawn->IsLocallyControlled())
		{
			//PlayWeaponSound(ReloadSound);
		}
	}
}

void ABoiWeapon::StopReload()
{
	if (CurrentState == EWeaponState::Reloading)
	{
		bReloading = false;
		DetermineWeaponState();
		GetWorldTimerManager().ClearTimer(TimerHandle_StopReload);
		GetWorldTimerManager().ClearTimer(TimerHandle_Reload);
	}
}

void ABoiWeapon::Reload()
{
	int32 ClipDelta = FMath::Min(MaxAmmoInGun - CurrentAmmoInWeapon, CurrentAmmoInBackpack);

	if (ClipDelta > 0)
	{
		CurrentAmmoInWeapon += ClipDelta;
		CurrentAmmoInBackpack -= ClipDelta;
	}
}

void ABoiWeapon::OnRep_bReloading()
{
	if (bReloading)
	{
		StartReload(true);
	}
	else
	{
		StopReload();
	}
}

void ABoiWeapon::ServerStartReload_Implementation()
{
	StartReload();
}

bool ABoiWeapon::ServerStartReload_Validate()
{
	return true;
}

void ABoiWeapon::ServerStopReload_Implementation()
{
	StopReload();
}

bool ABoiWeapon::ServerStopReload_Validate()
{
	return true;
}

void ABoiWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABoiWeapon, HitScanTrace);

	DOREPLIFETIME_CONDITION(ABoiWeapon, CurrentAmmoInWeapon, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABoiWeapon, CurrentAmmoInBackpack, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(ABoiWeapon, bReloading, COND_SkipOwner);
}