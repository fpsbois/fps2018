// Copyright © FPSBois 2018

#include "BoiWeaponDrop.h"

#include "Components/SphereComponent.h"
#include "Net/UnrealNetwork.h"

#include "BoiPlayerCharacter.h"
#include "BoiWeapon.h"

ABoiWeaponDrop::ABoiWeaponDrop()
{
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetCollisionObjectType(ECC_WorldDynamic);
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	RootComponent = SphereComp;

	SetReplicates(true);
	SetReplicateMovement(true);
}

void ABoiWeaponDrop::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SphereComp->OnComponentBeginOverlap.AddDynamic(this, &ABoiWeaponDrop::OnBeginOverlap);
	SphereComp->OnComponentEndOverlap.AddDynamic(this, &ABoiWeaponDrop::OnEndOverlap);
}

void ABoiWeaponDrop::SetWeapon(AActor* Instigator, ABoiWeapon* Weapon)
{
	if (m_Weapon)
	{
		m_Weapon->Destroy();
	}

	m_Weapon = Weapon;
	m_Weapon->AttachToActor(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	m_Weapon->SetActorRelativeLocation(FVector::ForwardVector * 2);

	FCollisionQueryParams QueryParams;
	QueryParams.bTraceComplex = false;

	if (Instigator)
	{
		QueryParams.AddIgnoredActor(Instigator);
	}

	FHitResult Hit;
	FVector TraceStart = GetActorLocation();
	FVector TraceEnd = TraceStart - FVector::UpVector * 10000;

	if (!GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Visibility, QueryParams))
	{
		if (m_Weapon)
		{
			m_Weapon->Destroy();
		}

		Destroy();
	}

	SetActorLocationAndRotation(Hit.ImpactPoint, Hit.ImpactNormal.ToOrientationQuat());
	GetWorldTimerManager().SetTimer(TimerHandle_EndLife, this, &ABoiWeaponDrop::DestroyWeaponDrop, m_LifeSpan, false);
}

ABoiWeapon* ABoiWeaponDrop::GetWeapon()
{
	return m_Weapon;
}

ABoiWeapon* ABoiWeaponDrop::SwitchWeapon(ABoiWeapon *Weapon)
{
	if (!Weapon) 
	{
		return nullptr;
	}

	ABoiWeapon* Ret = m_Weapon;

	m_Weapon = Weapon;
	m_Weapon->AttachToActor(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	m_Weapon->SetActorRelativeLocation(FVector::ForwardVector * 2);

	GetWorldTimerManager().SetTimer(TimerHandle_EndLife, this, &ABoiWeaponDrop::DestroyWeaponDrop, m_LifeSpan, false);

	return Ret;
}


// Called when the game starts or when spawned
void ABoiWeaponDrop::BeginPlay()
{
	Super::BeginPlay();

	if (Role == ROLE_Authority)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		if (DefaultWeapon && !m_Weapon) 
		{
			m_Weapon = GetWorld()->SpawnActor<ABoiWeapon>(DefaultWeapon, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
			if (m_Weapon)
			{
				m_Weapon->AttachToActor(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
				m_Weapon->SetActorRelativeLocation(FVector::ForwardVector*2);
			}
		}

		FCollisionQueryParams QueryParams;
		QueryParams.bTraceComplex = false;

		FHitResult Hit;
		FVector TraceStart = GetActorLocation();
		FVector TraceEnd = TraceStart - FVector::UpVector * 10000;

		if (!GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Visibility, QueryParams))
		{
			if (m_Weapon)
			{
				m_Weapon->Destroy();
			}

			Destroy();
		}

		SetActorLocationAndRotation(Hit.ImpactPoint, Hit.ImpactNormal.ToOrientationQuat());

		GetWorldTimerManager().SetTimer(TimerHandle_EndLife, this, &ABoiWeaponDrop::DestroyWeaponDrop, m_LifeSpan, false);
	}
}

void ABoiWeaponDrop::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (HasAuthority())
	{
		ABoiPlayerCharacter* PlayerChar = Cast<ABoiPlayerCharacter>(OtherActor);
		if (PlayerChar)
		{
			PlayerChar->SetWeaponDrop(this);
		}
	}
}

void ABoiWeaponDrop::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (HasAuthority())
	{
		ABoiPlayerCharacter* PlayerChar = Cast<ABoiPlayerCharacter>(OtherActor);
		if (PlayerChar && PlayerChar->GetWeaponDrop() == this)
		{
			PlayerChar->SetWeaponDrop(nullptr);
		}
	}
}

void ABoiWeaponDrop::DestroyWeaponDrop()
{
	if (m_Weapon)
	{
		m_Weapon->Destroy();
	}
	Destroy();
}

void ABoiWeaponDrop::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABoiWeaponDrop, m_Weapon);
}