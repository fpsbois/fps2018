// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BoiCreateAClassCard.generated.h"

class UBoiCreateAClassMenu;
class UBoiCreateAClassSave;
class UBoiWeaponDataAsset;

UCLASS()
class FPS2018_API UBoiCreateAClassCard : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	void Setup(UBoiCreateAClassMenu* NewCreateAClassMenu, UBoiCreateAClassSave* NewSaveSlot);

	UFUNCTION(BlueprintCallable)
	void UpdatePrimaryWeapon(int WeaponIndex);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdatePrimaryGraphics();

	UFUNCTION(BlueprintCallable)
	void UpdateSecondaryWeapon(int WeaponIndex);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateSecondaryGraphics();

protected:

	UPROPERTY(BlueprintReadOnly)
	UBoiCreateAClassMenu* CreateAClassMenu;

	UPROPERTY(BlueprintReadOnly)
	UBoiCreateAClassSave* SaveSlot;

	UPROPERTY(BlueprintReadOnly)
	int PrimaryWeaponIndex;
	
	UPROPERTY(BlueprintReadOnly)
	int SecondaryWeaponIndex;

	UPROPERTY(BlueprintReadOnly)
	UBoiWeaponDataAsset* CurrentPrimaryWeapon;

	UPROPERTY(BlueprintReadOnly)
	UBoiWeaponDataAsset* CurrentSecondaryWeapon;
	
};
