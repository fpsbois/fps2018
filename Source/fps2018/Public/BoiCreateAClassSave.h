// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "BoiCreateAClassSave.generated.h"

class ABoiWeapon;

/**
 * Store saves for Create A Class custom classes
 */
UCLASS()
class FPS2018_API UBoiCreateAClassSave : public USaveGame
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int PrimaryWeaponIndex;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int SecondaryWeaponIndex;
	
};
