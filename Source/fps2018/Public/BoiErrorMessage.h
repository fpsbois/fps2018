// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BoiErrorMessage.generated.h"

/**
 * 
 */
UCLASS()
class FPS2018_API UBoiErrorMessage : public UUserWidget
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadOnly, Category = "ErrorMessage")
	FString Message;
	
	UFUNCTION(BlueprintCallable, Category = "ErrorMessage")
	void ConfirmMessage();

public:

	void SetMessage(FString NewMessage) { Message = NewMessage; }

};
