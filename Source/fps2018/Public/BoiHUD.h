// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BoiHUD.generated.h"

/**
 * 
 */
UCLASS()
class FPS2018_API UBoiHUD : public UUserWidget
{
	GENERATED_BODY()

protected:

	virtual void NativeConstruct() override;
	
	
};
