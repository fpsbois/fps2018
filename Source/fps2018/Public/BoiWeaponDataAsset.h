// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Engine/Texture2D.h"
#include "BoiWeaponDataAsset.generated.h"

UCLASS(BlueprintType)
class FPS2018_API UBoiWeaponDataAsset : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString Name;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTexture2D* Thumbnail;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<class ABoiWeapon> Blueprint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTexture2D* KillfeedIcon;
};
