// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "BoiWeaponLibrary.generated.h"

class UBoiWeaponDataAsset;

UCLASS(BlueprintType)
class FPS2018_API UBoiWeaponLibrary : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<UBoiWeaponDataAsset*> PrimaryWeapons;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<UBoiWeaponDataAsset*> SecondaryWeapons;
			
};
