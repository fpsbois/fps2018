// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "BoiHealthComponent.generated.h"

// OnHealthChangedEvent
DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(FOnHealthChanged, UBoiHealthComponent*, HealthComp, float, Health, float, HealthDelta, const class UDamageType*, DamageType, class AController*, InstigatedBy, AActor*, DamageCauser);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnHealthDead, const class UDamageType*, DamageType, class AController*, InstigatedBy, AActor*, DamageCauser);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FPS2018_API UBoiHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	// Sets default values for this component's properties
	UBoiHealthComponent();


	UFUNCTION(BlueprintPure, Category = "HealthComponent")
	float GetCurrentHealth() const { return CurrentHealth; };

	UFUNCTION(BlueprintPure, Category = "HealthComponent")
	float GetMaxHealth() const { return MaxHealth; };

	/* True if health is <= 0 */
	UFUNCTION(BlueprintPure, Category = "HealthComponent")
	bool IsDead() const { return CurrentHealth <= 0.0f; }

	/* Select the team of the component */
	void SetTeam(uint8 Team) { TeamNum = Team; };


	/* Event broadcast when health is changed */
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnHealthChanged OnHealthChanged;

	/* Event broadcast when health drops <= 0 */
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnHealthDead OnHealthDead;

	/* Give the given amount of health to the player, clamped between current health and max health */
	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	void Heal(float HealAmount);

	/* Static function for checking if two actors are friendly by comparing teams, defaults to false */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HealthComponent")
	static bool IsFriendly(AActor* ActorA, AActor* ActorB);


protected:

	/* Widget for showing damage */
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "HealthComponent")
	uint8 TeamNum;

	/* Widget for showing damage */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HealthComponent")
	float MaxHealth;

	/* Current amount of health. 0 <= CurrentHealth <= MaxHealth */
	UPROPERTY(ReplicatedUsing = OnRep_Health, BlueprintReadOnly, Category = "HealthComponent")
	float CurrentHealth;

	UFUNCTION(NetMulticast, Reliable)
	void Multicast_Die(const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	/* Client side handling of change in health */
	UFUNCTION()
	void OnRep_Health(float OldHealth);

	virtual void BeginPlay() override;

	/*
	* Ends gameplay for this component.
	* Called from AActor::EndPlay only if bHasBegunPlay is true
	*/
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/* Hook to owning actors take any damage */
	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	

/************************************************************************/
/*                      AUTO HEALING FUNCTIONALITY                      */
/************************************************************************/
protected:

	/* How much health gained per second when healing */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HealthComponent")
	float HealthRegenAmount;

	/* How often a chunk of the HealthRegenAmoun is healed when healing */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HealthComponent")
	float HealthRegenTick;

	/* How long after being shot until healing starts */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HealthComponent")
	float NonCombatTime;

	FTimerHandle TimerHandle_NonCombat;
	FTimerHandle TimerHandle_Healing;

	UFUNCTION()
	void OnHealingTick();

	UFUNCTION()
	void StartHealing();

};
