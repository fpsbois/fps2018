// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"

#include "BoiNameWidgetComponent.generated.h"

UCLASS()
class FPS2018_API UBoiNameWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()
	
public:

	UBoiNameWidgetComponent();

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	
	void ActivateNameComponent(bool bTeammate);

	void DeactivateNameComponent();

	void SetName(FString NewName);

protected:

	UFUNCTION()
	void OnOwnerDied(const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
	void OnRep_Name();

	UPROPERTY(EditDefaultsOnly, Category = "NameWidgetComponent")
	float DeactivateAfterXSeconds = 1;

	APawn* Target;

	bool bActive = false;

	FTimerHandle TimerHandle_DeactivationTimer;

	UPROPERTY(ReplicatedUsing = OnRep_Name)
	FString Name;
};
