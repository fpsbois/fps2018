// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "BoiPlayerDetectComponent.generated.h"

UENUM(BlueprintType)
enum class EDetectionType : uint8
{
	Default 	UMETA(DisplayName = "Default"),
	Enemy 		UMETA(DisplayName = "Enemy"),
	Ally		UMETA(DisplayName = "Ally"),
	Objective	UMETA(DisplayName = "Objective")
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FPS2018_API UBoiPlayerDetectComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBoiPlayerDetectComponent();

	UFUNCTION(Client, Reliable)
	void Client_ActivateDetection();

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Player Detector")
	float VacinityDetectionDistance;

	UPROPERTY(EditDefaultsOnly, Category = "Player Detector")
	float LookDetectionDistance;

	UPROPERTY(EditDefaultsOnly, Category = "Player Detector")
	float RefreshRate = 0.4f;

	// Called when the game starts
	virtual void BeginPlay() override;

	FTimerHandle TimerHandle_DetectionTimer;

	void OnDetectionTimerTick();

	void CheckTowardsFacingDirection();

	void CheckForTeam();
	
};
