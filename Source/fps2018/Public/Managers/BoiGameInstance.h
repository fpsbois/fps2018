// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"

#include "MenuInterface.h"
#include "BoiEnumsStructs.h"

#include "BoiGameInstance.generated.h"

class UUserWidget;

UCLASS()
class FPS2018_API UBoiGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

private:

	/* Path to Main Menu Map */
	const TCHAR* MAIN_MENU_MAP_PATH = TEXT("/Game/FPSBois/Maps/MainMenu");

	/** Path to Main Menu UserWidget */
	const TCHAR* MAIN_MENU_CLASS_PATH = TEXT("/Game/FPSBois/UI/Menus/WBP_Menu_Main");

	/** Path to InGame Menu UserWidget */
	const TCHAR* INGAME_MENU_CLASS_PATH = TEXT("/Game/FPSBois/UI/Menus/WBP_Menu_InGame");

	bool bSteam = false;

protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameInstance")
	TArray<FMapInfo> Maps;

	UPROPERTY(EditDefaultsOnly, Category = "UserWidgets")
	TSubclassOf<UUserWidget> LoadingScreenClass;

	UPROPERTY(EditDefaultsOnly, Category = "UserWidgets")
	TSubclassOf<UUserWidget> ErrorMessageClass;

	UPROPERTY(EditDefaultsOnly, Category = "MenuWidgets")
	TSubclassOf<UUserWidget> CreateAClassMenuClass;

	UPROPERTY(EditDefaultsOnly, Category = "GameInstance")
	UBoiWeaponLibrary* WeaponLibrary;

	UUserWidget* LoadingScreenWidget;

	UFUNCTION()
	void OnNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString);

	UFUNCTION()
	void OnTravelFailure(UWorld* World, ETravelFailure::Type FailureType, const FString& ErrorString);

public:

	UFUNCTION()
	virtual void BeginLoadingScreen(const FString& MapName);

	UFUNCTION()
	virtual void EndLoadingScreen(UWorld* InLoadedWorld);

	UFUNCTION(BlueprintPure, Category = "GameInstance")
	FString GetMapName(int MapID) const;

	UFUNCTION(BlueprintPure, Category = "GameInstance")
	UTexture2D* GetMapImage(int MapID) const;

	UFUNCTION(BlueprintPure, Category = "GameInstance")
	FGameSettings GetGameSettings() const { return GameSettings; };

	UFUNCTION(BlueprintPure, Category = "GameInstance")
	UBoiWeaponLibrary* GetWeaponLibrary() const { return WeaponLibrary;	};


	/// ----- MENU HANDLING METHODS -----
	UFUNCTION(BlueprintCallable)
	void ShowMainMenu();

	UFUNCTION(BlueprintCallable)
	void ShowInGameMenu();

	UFUNCTION(BlueprintCallable)
	void ShowCreateAClassMenu();

	void DestroySession(APlayerController* PlayerController);

	void StartGame(FGameSettings Settings);


	/// ----- Inherited via IMenuInterface -----
	virtual void Host(FGameSettings Settings) override;
	virtual void Join(uint32 Index) override;
	virtual void QuitToMainMenu() override;
	virtual void QuitToDesktop() override;
	
	UFUNCTION(BlueprintCallable, Category = "Networking")
	void RefreshServerList();

private:

/// ----- MENU CLASS TYPES -----

	/** Holds InGame Menu UserWidget Class type */
	TSubclassOf<class UUserWidget> InGameMenuClass;
	/** Holds Main Menu UserWidget Class type */
	TSubclassOf<class UUserWidget> MainMenuClass;


/// ----- NETWORKING PROPERTIES -----
	/** Contains data about a search for game sessions */
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	/** Interface for accessing the session management services */
	IOnlineSessionPtr SessionInterface;

/// ----- OTHER PRIVATE VARIABLES

	/** Pointer to the Main Menu created from MenuClass */
	class UBoiMainMenu* MainMenu;

	FGameSettings GameSettings;

/// ----- PRIVATE FUNCTIONS -----

	UBoiGameInstance(const FObjectInitializer &ObjectInitializer);
	void Init() override;

	void OnCreateSessionComplete(FName SessionName, bool Success);
	void OnDestroySessionComplete(FName SessionName, bool Success);
	void OnFindSessionsComplete(bool Success);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

};
