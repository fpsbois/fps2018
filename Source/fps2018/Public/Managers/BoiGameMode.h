// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "BoiGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnActorKilled, AActor*, VictimActor, AController*, VictimController, AActor*, KillerActor, AController*, KillerController);

class ABoiPlayerController;
class APlayerStart;

UCLASS()
class FPS2018_API ABoiGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABoiGameMode();

	virtual void StartPlay() override;

	/* Broadcast whenever an actor is killed. */
	UPROPERTY(BlueprintAssignable, Category = "GameMode")
	FOnActorKilled OnActorKilled;

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void Logout(AController* Exiting) override;

	virtual void SwapPlayerControllers(APlayerController* OldPC, APlayerController* NewPC) override;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "GameMode")
	void Respawn(AController* Controller);


//////////////////////////////////////////////////////////////////////////
//                             UTILITIES			    				//
//////////////////////////////////////////////////////////////////////////
protected:

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void RespawnPlayerAtPlayerStart(ABoiPlayerController* PlayerController, APlayerStart* PlayerStart);

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void RespawnPlayerAtPlayerStartWithTimer(ABoiPlayerController* PlayerController, APlayerStart* PlayerStart, float timer);

	UFUNCTION(BlueprintPure, Category = "GameMode")
	bool DidKillFriendly(AActor* VictimActor, AController* VictimController, AActor* KillerActor, AController* KillerController) const;

	UFUNCTION(BlueprintPure, Category = "GameMode")
	bool DidKillSelf(AActor* VictimActor, AController* VictimController, AActor* KillerActor, AController* KillerController) const;


//////////////////////////////////////////////////////////////////////////
//                          GAME MODE SETTINGS							//
//////////////////////////////////////////////////////////////////////////
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoiGameMode")
	int32 ScoreLimit;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoiGameMode")
	int32 TimeLimit;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoiGameMode")
	int32 RespawnTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoiGameMode")
	bool bAllowNegativeScores;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoiGameMode")
	int32 PointsFromSuicide;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoiGameMode")
	int32 PointsFromKill;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoiGameMode")
	int32 PointsFromTeamkill;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BoiGameMode")
	int32 PointsFromAssist;
	
protected:

	TArray<APlayerController*> AllPlayerControllers;

	// Start spawning players
	void StartGame();

	// Stop spawning players
	void EndGame();

	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void AssignNewPlayerToTeam(APlayerController* NewPlayerController, ABoiPlayerState* NewPlayerState);

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void AddPlayerBalanced(APlayerController* NewPlayerController, ABoiPlayerState* NewPlayerState);

	/* Handle Game Mode defined rules in Blueprint with this. Called when OnActorKilled is broadcast. */
	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void ActorKilled(AActor* VictimActor, AController* VictimController, AActor* KillerActor, AController* KillerController);

	FTimerHandle TimerHandle_PlayerSpawner;
	
	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void StartSpawnFromQueueTimer();

	void SpawnPlayerFromQueue();

	UPROPERTY(BlueprintReadOnly, Category = "GameMode")
	TArray<AController*> DeadPlayerQueue;

	FTimerHandle TimerHandle_GameTimerTick;

	void OnGameTimerTick();
};
