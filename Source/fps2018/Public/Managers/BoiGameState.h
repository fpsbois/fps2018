// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"

#include "BoiEnumsStructs.h"
#include "BoiGameMode.h"

#include "BoiGameState.generated.h"

class ABoiPlayerState;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerTeamChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnScoreChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTimeRemainingChanged);

UCLASS()
class FPS2018_API ABoiGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	ABoiGameState();

	UFUNCTION(BlueprintCallable, Category = "GameState")
	int32 AddScoreToPlayersTeam(AController* Controller, int32 DeltaScore);

	UFUNCTION(BlueprintCallable, Category = "GameState")
	void AddScore(uint8 Team, int32 DeltaScore);

	UFUNCTION(BlueprintCallable, Category = "GameState")
	void AddTime(int32 Time);

	UPROPERTY(BlueprintAssignable, Category = "GameState")
	FOnScoreChanged OnScoreChanged;

	UPROPERTY(BlueprintAssignable, Category = "GameState")
	FOnTimeRemainingChanged OnTimeRemainingChanged;

	UPROPERTY(BlueprintAssignable, Category = "GameState")
	FOnPlayerTeamChanged OnPlayerTeamChanged;

	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnPlayerTeamChanged();

	UPROPERTY(BlueprintAssignable, Category = "GameState")
	FOnActorKilled OnActorKilled;

	UFUNCTION(BlueprintCallable, Category = "GameState")
	void AddPlayerToTeam(AController* Controller, uint8 Team);
	
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_TimeRemainingChanged, Category = "GameState")
	int32 TimeRemaining;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "GameState")
	int32 TimeLimit;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "GameState")
	int32 ScoreLimit;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "GameState")
	EGameMode GameMode;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "GameState")
	int32 MaxPlayerCount;

	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnEndRound();

	UFUNCTION(NetMulticast, Reliable, Category = "GameState")
	void MulticastActorKilled(AActor* VictimActor, AController* VictimController, AActor* KillerActor, AController* KillerController);

	int32 GetTeamSize(int32 Team);

protected:

	UFUNCTION()
	void OnRep_TimeRemainingChanged();

	UFUNCTION()
	void OnRep_TeamScoreChanged();

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_TeamScoreChanged, Category = "GameState")
	TArray<int32> TeamScores;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "GameState")
	TArray<int32> TeamSizes;
};
