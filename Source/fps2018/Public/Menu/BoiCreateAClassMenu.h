	// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"

#include "Menu/MenuWidget.h"

#include "BoiCreateAClassMenu.generated.h"

class UBoiCreateAClassSave;
class UBoiWeaponDataAsset;

UCLASS()
class FPS2018_API UBoiCreateAClassMenu : public UMenuWidget
{
	GENERATED_BODY()

public:
	
	UBoiCreateAClassMenu();
	
	void SaveAllClasses();
	
	bool LoadSavedClasses();

	void CreateEmptySavedClassess();
	
protected:

	virtual void NativePreConstruct() override;

	UPROPERTY(EditDefaultsOnly, Category = "CreateAClassMenu")
	TSubclassOf<UBoiCreateAClassSave> CreateAClassSaveClass;

	UPROPERTY(VisibleAnywhere, Category = "CreateAClassMenu")
	FString Slot1Name;

	UPROPERTY(VisibleAnywhere, Category = "CreateAClassMenu")
	FString Slot2Name;

	UPROPERTY(VisibleAnywhere, Category = "CreateAClassMenu")
	FString Slot3Name;

	UPROPERTY(BlueprintReadOnly)
	UBoiCreateAClassSave* SaveSlot1;

	UPROPERTY(BlueprintReadOnly)
	UBoiCreateAClassSave* SaveSlot2;

	UPROPERTY(BlueprintReadOnly)
	UBoiCreateAClassSave* SaveSlot3;


};
