// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"

#include "BoiInGameMenu.generated.h"

UCLASS()
class FPS2018_API UBoiInGameMenu : public UMenuWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(meta = (BindWidget))
	class UButton* QuitButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* MainMenuButton;
	
	UPROPERTY(meta = (BindWidget))
	class UButton* ResumeButton;

protected:
	virtual bool Initialize();

private:

	UFUNCTION()
	void ResumeGame();

	UFUNCTION()
	void QuitToMainMenu();

	UFUNCTION()
	void QuitGame();
};
