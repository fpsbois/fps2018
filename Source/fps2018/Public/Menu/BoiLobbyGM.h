// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "BoiEnumsStructs.h"

#include "BoiLobbyGM.generated.h"

/**
 * Game Mode for a Lobby Map. Game Mode only exists on the server.
 */
UCLASS()
class FPS2018_API ABoiLobbyGM : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	ABoiLobbyGM();

	/** Called before initializing components. */
	virtual void PreInitializeComponents() override;

	/** Called after a successful login.  This is the first place it is safe to call replicated functions on the PlayerController. */
	virtual void PostLogin(APlayerController* NewPlayer) override;

	/** Called when a Controller with a PlayerState leaves the game or is destroyed */
	virtual void Logout(AController* Exiting) override;



	/** Returns whether or not the game can start */
	UFUNCTION(BlueprintPure, Category = "Lobby Game Mode")
	bool GetCanStartGame() const { return bCanStartGame; };

	/** Returns all connected player controllers */
	UFUNCTION(BlueprintPure, Category = "Lobby Game Mode")
	TArray<APlayerController*> GetAllPlayerControllers() const { return AllPlayerControllers; };


	/** Begin the game. */
	void StartGame();

	/** Update the player infos on all clients */
	void UpdateAllClients();

	/** Change the game settings to the given game settings, send changes to all clients */
	UFUNCTION(BlueprintCallable)
	void ChangeGameSettings(int NewMapTime, int MapID, int NewScoreLimit, EGameMode NewGameMode);


private:

	// Can the lobby start a game
	bool bCanStartGame = false;

	// Struct of all game settings
	FGameSettings GameSettings;

	// Array of structs containing player info
	TArray<FPlayerInfo> ConnectedPlayersInfo;
	
	// Array of connected player controllers
	TArray<APlayerController*> AllPlayerControllers;
};
