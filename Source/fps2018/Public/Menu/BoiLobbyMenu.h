// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "MenuWidget.h"

#include "BoiLobbyMenu.generated.h"

class ABoiLobbyPC;
class UButton;
class UScrollBox;
class UBoiPlayerCard;

UCLASS()
class FPS2018_API UBoiLobbyMenu : public UMenuWidget
{
	GENERATED_BODY()

public:

	virtual void NativeConstruct() override;

	void ClearPlayerLists();
	
	UFUNCTION(Client, Reliable)
	void Client_AddPlayerCard(FPlayerInfo PlayerInfo);

public:

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "LobbyInfo")
	FText LobbyServerName;
	
	UPROPERTY(BlueprintReadWrite, Replicated, Category = "LobbyInfo")
	FText MapName;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "LobbyInfo")
	int TimeLimit;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "LobbyInfo")
	int ScoreLimit;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Replicated, Category = "LobbyMenu")
	UTexture2D* MapImage;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "LobbyInfo")
	int CurrentPlayers;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "LobbyInfo")
	int MaxPlayers;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "LobbyInfo")
	FText ReadyButtonText;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "LobbyInfo")
	FText ReadyStatus;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "LobbyInfo")
	int MapID;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "LobbyInfo")
	EGameMode GameMode;

protected:

	UFUNCTION(BlueprintCallable)
	void StartGame();

	UFUNCTION(BlueprintCallable)
	void ReadyUp();

	UFUNCTION(BlueprintCallable)
	void SwapTeams();

	UFUNCTION(BlueprintCallable, Category = "LobbyMenu")
	void LeaveGame();

private:

	bool bReady = false;

	ABoiLobbyPC* LocalController;

protected:

	UPROPERTY(EditDefaultsOnly, Category = "LobbyMenu")
	TSubclassOf<UBoiPlayerCard> PlayerCardClass;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget), Category = "LobbyMenu")
	UScrollBox* TeamOneList;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget), Category = "LobbyMenu")
	UScrollBox* TeamTwoList;

};
