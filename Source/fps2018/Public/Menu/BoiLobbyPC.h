// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "BoiGameInstance.h"
#include "BoiEnumsStructs.h"
#include "BoiPlayerSaveGame.h"

#include "BoiLobbyPC.generated.h"

class UBoiLobbyMenu;
class UBoiPlayerSaveGame;

UCLASS()
class FPS2018_API ABoiLobbyPC : public APlayerController
{
	GENERATED_BODY()

private:

	/* Path to Main Menu Map */
	const TCHAR* PLAYER_SAVE_GAME_PATH = TEXT("/Game/FPSBois/Core/BP_PlayerSaveGame");

	TSubclassOf<UBoiPlayerSaveGame> SaveGameClass;

public:

	ABoiLobbyPC();

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintPure)
	FPlayerInfo GetPlayerSettings() const { return PlayerSettings; }
	
public:

	UFUNCTION(Client, Reliable)
	void Client_InitialSetup(uint8 Team);

	UFUNCTION(Client, Reliable)
	void Client_Teardown();

	UFUNCTION(Client, Reliable)
	void Client_SetupLobbyMenu(const FText& ServerName);

	UFUNCTION(Client, Reliable)
	void Client_SetupLoadingScreen();

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
	void Server_CallUpdate(FPlayerInfo PlayerInfo);

	UFUNCTION(Client, Reliable)
	void Client_AddPlayerInfo(const TArray<FPlayerInfo>& ConnectedPlayersInfo);

	UFUNCTION(Client, Reliable)
	void Client_UpdateLobbySettings(int MapID, int MapTime, int ScoreLimit, EGameMode GameMode);

	UFUNCTION(Client, Reliable)
	void Client_Kick();

	UFUNCTION(Client, Reliable)
	void Client_UpdateNumberOfPlayers(int CurrentPlayers, int MaxPlayers);

private:

	/** Widget to display on all clients when the game is loading. */
	UPROPERTY(EditDefaultsOnly, Category = "LobbyPC")
	TSubclassOf<UUserWidget> LoadingScreenClass;

	UPROPERTY(Replicated)
	UBoiLobbyMenu* LobbyMenuWidget;

	UPROPERTY(EditDefaultsOnly, Category = "LobbyPC")
	TSubclassOf<UUserWidget> LobbyMenuClass;

	UPROPERTY(EditDefaultsOnly, Category = "LobbyPC")
	FString PlayerSettingsSaveSlotName;

public:

	UPROPERTY(Replicated)
	FPlayerInfo PlayerSettings;

	UPROPERTY(Replicated)
	TArray<FPlayerInfo> AllConnectedPlayers;


private:

	UBoiPlayerSaveGame* PlayerSaveGame;

	void SaveGameCheck();

	void SaveGame();

	void LoadGame();

	UTexture2D* GetSteamAvatar();

	void UpdateName();
	
};
