// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "fps2018.h"
#include "MenuWidget.h"
#include "MenuInterface.h"

#include "BoiMainMenu.generated.h"

USTRUCT()
struct FServerData
{
	GENERATED_BODY()

	FString Name;
	uint16 CurrentPlayers;
	uint16 MaxPlayers;
	FString HostUsername;
};

UCLASS()
class FPS2018_API UBoiMainMenu : public UMenuWidget
{
	GENERATED_BODY()
	
public:

	TSubclassOf<class UUserWidget> ServerRowClass;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* MenuSwitcher;



	/// ----- MAIN MENU PROPERTIES -----
	UPROPERTY(meta = (BindWidget))
	class UWidget* MainMenu;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton* HostButton;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton* JoinButton;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton* QuitButton;



	/// ----- JOIN MENU PROPERTIES -----
	UPROPERTY(meta = (BindWidget))
	class UWidget* JoinMenu;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UWidget* SearchingThrobber;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton* JoinMenuBackButton;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton* JoinMenuSearchButton;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UPanelWidget* ServerList;

	UPROPERTY(EditAnywhere, Category = "Appearance")
	FLinearColor ServerListSelectedColor;

	UPROPERTY(EditAnywhere, Category = "Appearance")
	FLinearColor ServerListDefaultColor;



	/// ----- HOST MENU PROPERTIES -----
	UPROPERTY(meta = (BindWidget))
	class UWidget* HostMenu;

	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* ServerNameTextBox;

	UPROPERTY(meta = (BindWidget))
	class UButton* HostMenuCancelButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* HostMenuHostButton;

	UPROPERTY(BlueprintReadWrite)
	int PlayerCount = 4;

	/// ----- PUBLIC FUNCTIONS -----
	void SetServerList(TArray<FServerData> ServerNames);
	void SelectIndex(uint32 Index);
	

protected:
	virtual bool Initialize();


private:
	/// ----- PRIVATE PROPERTIES -----
	TOptional<uint32> SelectedIndex;

	/// ----- PRIVATE METHODS -----
	UBoiMainMenu(const FObjectInitializer &ObjectInitializer);

	UFUNCTION()
	void OpenMainMenu();

	UFUNCTION()
	void OpenHostMenu();

	UFUNCTION()
	void OpenJoinMenu();

	UFUNCTION()
	void HostServer();

	UFUNCTION()
	void JoinServer();

	UFUNCTION()
	void QuitGame();

	void UpdateServerListSelection();

};
