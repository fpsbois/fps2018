// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "BoiEnumsStructs.h"

#include "BoiPlayerCard.generated.h"

class UTextBlock;
class UImage;

/**
 * 
 */
UCLASS()
class FPS2018_API UBoiPlayerCard : public UUserWidget
{
	GENERATED_BODY()

public:

	void Setup(FPlayerInfo PlayerSettings);

protected:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget), Category = "PlayerCard")
	UTextBlock* PlayerNameTextBlock;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget), Category = "PlayerCard")
	UTextBlock* PlayerStatusTextBlock;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget), Category = "PlayerCard")
	UImage* PlayerImage;

protected:

	UPROPERTY(BlueprintReadOnly, Replicated)
	FText PlayerName;

	UPROPERTY(BlueprintReadOnly, Replicated)
	UTexture2D* PlayerImageTexture;

	UPROPERTY(BlueprintReadOnly, Replicated)
	FText PlayerStatus;
};
