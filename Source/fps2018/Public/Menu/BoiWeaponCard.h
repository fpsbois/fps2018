// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BoiWeaponCard.generated.h"

class UBoiCreateAClassMenu;
class UBoiCreateAClassSave;

UENUM(BlueprintType)
enum class EWeaponCardType : uint8
{
	Primary 	UMETA(DisplayName = "Primary"),
	Secondary 	UMETA(DisplayName = "Secondary")
};

/**
 * User Widget for displaying Weapon info.
 */
UCLASS()
class FPS2018_API UBoiWeaponCard : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	void Setup(UBoiCreateAClassMenu* NewCreateAClassMenu, UBoiCreateAClassSave* NewCreateAClassSave);
		
protected:

	UPROPERTY(BlueprintReadWrite)
	UBoiCreateAClassMenu* CreateAClassMenu;

	UPROPERTY(BlueprintReadWrite)
	UBoiCreateAClassSave* SaveSlot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Index;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EWeaponCardType Type;
	
};
