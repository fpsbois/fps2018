#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "BoiEnumsStructs.h"

#include "MenuInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMenuInterface : public UInterface
{
	GENERATED_BODY()
};

class FPS2018_API IMenuInterface
{
	GENERATED_BODY()

public:
	virtual void Host(FGameSettings Settings) = 0;
	virtual void Join(uint32 Index) = 0;
	virtual void QuitToMainMenu() = 0;
	virtual void QuitToDesktop() = 0;
};
