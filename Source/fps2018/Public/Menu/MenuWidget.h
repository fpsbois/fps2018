// Copyright � FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "MenuInterface.h"

#include "MenuWidget.generated.h"


UCLASS()
class FPS2018_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	/// ----- PUBLIC FUNCTIONS -----
	virtual void Setup();

	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void Teardown();
	void SetMenuInterface(IMenuInterface* MenuInterface);
	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;

protected:
	IMenuInterface* MenuInterface;
};
