// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "BoiPlayerDetectComponent.h"

#include "BoiCrosshair.generated.h"

/**
 * 
 */
UCLASS()
class FPS2018_API UBoiCrosshair : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintImplementableEvent, Category = "Crosshair")
	void SetColorFromDetectionType(EDetectionType DetectionType);
};
