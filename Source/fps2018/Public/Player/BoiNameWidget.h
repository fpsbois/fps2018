// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "BoiNameWidget.generated.h"

class UTextBlock;

UCLASS()
class FPS2018_API UBoiNameWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent, Category = "NameWidget")
	void ShowTeamName();

	UFUNCTION(BlueprintImplementableEvent, Category = "NameWidget")
	void ShowEnemyName();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "NameWidget")
	void HideName();

	void SetPlayerNameText(FString Text);
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* PlayerNameText;
};
