// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "BoiPlayerCharacter.generated.h"

class UCharacterMovementComponent;
class UCameraComponent;
class UWidgetComponent;

class ABoiGrenade;
class UBoiHealthComponent;
class UBoiNameWidgetComponent;
class UBoiPlayerDetectComponent;
class ABoiWeapon;
class ABoiWeaponDrop;

UCLASS(Abstract, HideCategories = ("Actor Tick", "Input", "Replication"))
class FPS2018_API ABoiPlayerCharacter : public ACharacter
{
	GENERATED_BODY()

protected:

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName WeaponSocketName = "WeaponSocket";
	FName WeaponSocketNameFP = "WeaponSocketFP";



	/* Default move speed */
	UPROPERTY(EditDefaultsOnly, Category = "Movement", meta = (ClampMin = "0", UIMin = "0"))
	float MoveSpeed = 600.0f;

	/* Move speed while aiming down sights */
	UPROPERTY(EditDefaultsOnly, Category = "Movement", meta = (ClampMin = "0", UIMin = "0"))
	float ADSMoveSpeed = 300.0f;

	/* Move speed while sprinting */
	UPROPERTY(EditDefaultsOnly, Category = "Movement", meta = (ClampMin = "0", UIMin = "0"))
	float SprintSpeed = 1200.0f;

	/* Move speed while crouched and not aiming down sights */
	UPROPERTY(EditDefaultsOnly, Category = "Movement", meta = (ClampMin = "0", UIMin = "0"))
	float CrouchSpeed = 300.0f;

	/* Move speed while crouched and aiming downs sights */
	UPROPERTY(EditDefaultsOnly, Category = "Movement", meta = (ClampMin = "0", UIMin = "0"))
	float CrouchSpeedADS = 200.0f;

	/* Move speed while prone */
	UPROPERTY(EditDefaultsOnly, Category = "Movement", meta = (ClampMin = "0", UIMin = "0"))
	float ProneSpeed = 150.0f;

	/* Collision half-height when crouching (component scale is applied separately) */
	UPROPERTY(EditDefaultsOnly, Category = "Movement", meta = (ClampMin = "0", UIMin = "0"))
	float CrouchedHalfHeight = 40;

	/* Collision half-height when prone (component scale is applied separately) */
	UPROPERTY(EditDefaultsOnly, Category = "Movement", meta = (ClampMin = "0", UIMin = "0"))
	float ProneHalfHeight = 20;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	bool bCanReloadWhileJumping = true;

	/* Weapon to spawn with */
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<ABoiWeapon> DefaultWeapon;


public:

	ABoiPlayerCharacter();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void PawnClientRestart() override;

	UFUNCTION(BlueprintPure, Category = "PlayerCharacter")
	bool IsSprinting() const { return bSprinting; }

	UFUNCTION(BlueprintPure, Category = "PlayerCharacter")
	bool IsProne() const { return bProne; }

	UFUNCTION(BlueprintPure, Category = "PlayerCharacter")
	bool IsCrouching() const;

	UFUNCTION(BlueprintPure, Category = "PlayerCharacter")
	bool IsFalling() const;

	UFUNCTION(BlueprintPure, Category = "PlayerCharacter")
	bool IsADS() const { return bAimDownSights; };

	UFUNCTION(BlueprintPure, Category = "PlayerCharacter")
	bool IsReloading() const;

	UFUNCTION(BlueprintPure, Category = "PlayerCharacter")
	bool IsFiring() const { return bFiring; };

	UFUNCTION(BlueprintPure, Category = "Components")
	ABoiWeapon* GetCurrentWeapon() const { return CurrentWeapon; };
		
	FName GetWeaponAttachPoint() const { return WeaponSocketName; }
	FName GetWeaponAttachPointFP() const { return WeaponSocketNameFP; }
	
	USkeletalMeshComponent* GetSpecifcPawnMesh(bool WantFirstPerson) const;

	USkeletalMeshComponent* GetPawnMesh() const;

	bool IsFirstPerson() const;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* FPPMeshComp = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UBoiNameWidgetComponent* NameWidgetComp = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UBoiPlayerDetectComponent* PlayerDetectComp = nullptr;
	
	bool bSprinting = false;

	UPROPERTY(Replicated)
	bool bAimDownSights = false;

	UPROPERTY(Replicated)
	bool bProne = false;

	UPROPERTY(ReplicatedUsing = OnRep_bDied)
	bool bDied = false;

	bool bFiring = false;

	UFUNCTION()
	void OnRep_bDied();
	
	UPROPERTY(BlueprintReadOnly, Replicated)
	float Pitch = 0.0f;

	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndplayReason) override;
	
	void UpdatePawnMeshes();

	void MoveForward(float Value);
	void MoveRight(float Value);
	void Turn(float Value);

	void CrouchToggle();

	UFUNCTION()
	void Interact();

	void Sprint();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerStartSprinting();

	void StopSprinting();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerStopSprinting();


	void LookUp(float Value);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerLookUp(float Value);
	
	void BeginADS();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerBeginADS();

	void EndADS();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerEndADS();

	void ProneToggle();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerProneToggle();

	void BeginFire();
	void EndFire();

	void Reload();

	virtual void Jump() override;

	void UpdateMovementSpeed();

	void SpawnDefaultInventory();


/************************************************************************/
/*                          ADS FUNCTIONALITY                           */
/************************************************************************/
private:
	// Default FOV when not ADS, initialized in PostInitializeComponents from CameraComp
	float FOVStorage = 90.0f;
	
	UPROPERTY(EditDefaultsOnly, Category = "PlayerCharacter")
	float DefaultADSSpeed = 20.0f;

/************************************************************************/
/*                    WEAPON SWAPPING FUNCTIONALITY                     */
/************************************************************************/
protected:

	UPROPERTY(Transient, Replicated)
	TArray<ABoiWeapon*> Inventory;

	void EquipWeapon(ABoiWeapon* Weapon);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerEquipWeapon(ABoiWeapon* NewWeapon);

	void SetCurrentWeapon(ABoiWeapon* NewWeapon, ABoiWeapon* LastWeapon = nullptr);

	void AddWeapon(ABoiWeapon* Weapon);

	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentWeapon)
	ABoiWeapon* CurrentWeapon = nullptr;

	UFUNCTION()
	void OnRep_CurrentWeapon(ABoiWeapon* LastWeapon);

	UFUNCTION()
	void EquipPrimary();

	UFUNCTION()
	void EquipSecondary();


/************************************************************************/
/*                      WEAPON DROP FUNCTIONALITY                       */
/************************************************************************/
public:

	void SetWeaponDrop(ABoiWeaponDrop* WeaponDrop);

	ABoiWeaponDrop* GetWeaponDrop() const { return WeaponDrop; };

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<ABoiWeaponDrop> WeaponDropClass;

	UPROPERTY(ReplicatedUsing = OnRep_WeaponDrop)
	ABoiWeaponDrop* WeaponDrop = nullptr;

	/* Switch weapons with a weapon drop */
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_InteractWithWeaponDrop();

	UFUNCTION()
	void OnRep_WeaponDrop();


	UPROPERTY(EditDefaultsOnly, Category = "CreateAClassMenu")
	TSubclassOf<class UBoiCreateAClassSave> CreateAClassSaveClass;

/************************************************************************/
/*                         HEALTH FUNCTIONALITY                         */
/************************************************************************/
 protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UBoiHealthComponent* HealthComp;

	UFUNCTION()
	void OnDied(const UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

/************************************************************************/
/*                        GRENADE FUNCTIONALITY                         */
/************************************************************************/
public:

	// Returns the current number of grenades in reserve
	UFUNCTION(BlueprintPure, Category = "Grenade")
	int GetGrenadeCount() const { return GrenadeCount; };

protected:

	// Default grenade to spawn with
	UPROPERTY(EditDefaultsOnly, Category = "Grenade", Replicated)
	TSubclassOf<ABoiGrenade> DefaultGrenade;

	// Number of grenades being held
	UPROPERTY(EditDefaultsOnly, Category = "Grenade", Replicated)
	int GrenadeCount = 2;

	// The grenade currently being held, i.e. pulled fuse but still holding, not thrown
	UPROPERTY(Replicated)
	ABoiGrenade* CurrentGrenade;

	// Pull the grenade on the fuse, but don't throw, i.e. cooking the grenade
	void PullGrenadeFuse();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_PullGrenadeFuse();

	// Throw the currently held grenade, if any
	void ThrowGrenade();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_ThrowGrenade();

};
