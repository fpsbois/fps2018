// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "BoiEnumsStructs.h"

#include "BoiPlayerController.generated.h"

enum class EDetectionType : uint8;
class UBoiCrosshair;

UCLASS()
class FPS2018_API ABoiPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent, Category = "PlayerController")
	void OnRoundComplete();

	UFUNCTION(BlueprintImplementableEvent, Category = "PlayerController")
	void OnShotActorWithHealth(bool bKilledActor);

	UPROPERTY(Replicated)
	FPlayerInfo PlayerSettings;

	void SetReticleColor(EDetectionType DetectionType);

	UFUNCTION(BlueprintCallable, Category = "PlayerController")
	void OnPlayerKilled(FString Killer, FString Victim);
	
	virtual void Possess(APawn* aPawn) override;
	virtual void UnPossess() override;

	UFUNCTION(Client, Reliable, Category = "PlayerController") 
	void Client_ClearHUDWidgets(); 

protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "PlayerController")
	void OnPossessed(APawn* aPawn);
	UFUNCTION(BlueprintImplementableEvent, Category = "PlayerController")
	void OnUnPossessed();

	UPROPERTY(EditDefaultsOnly, Category = "HUD")
	TSubclassOf<UBoiCrosshair> CrosshairClass;

	UPROPERTY(BlueprintReadOnly, Category = "HUD")
	UBoiCrosshair* Crosshair;

	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

	void OpenMenu();
};
