// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"

#include "BoiEnumsStructs.h"

#include "BoiPlayerSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class FPS2018_API UBoiPlayerSaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, Replicated, Category = "SaveGame")
	FPlayerInfo PlayerInfo;
	
};
