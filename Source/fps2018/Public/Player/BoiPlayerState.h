// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "Net/UnrealNetwork.h"

#include "BoiPlayerState.generated.h"

UCLASS()
class FPS2018_API ABoiPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	void SetTeam(uint8 TeamNumber);

	uint8 GetTeam() { return Team; }

protected:

	UFUNCTION(BlueprintCallable)
	void AddScore(float ScoreDelta);
	
	UPROPERTY(Replicated, BlueprintReadWrite)
	uint8 Team;
};
