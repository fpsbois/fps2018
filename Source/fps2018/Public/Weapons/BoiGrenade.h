// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "BoiGrenade.generated.h"

class USphereComponent;
class UStaticMeshComponent;
class UProjectileMovementComponent;
class URadialForceComponent;

UCLASS()
class FPS2018_API ABoiGrenade : public AActor
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	URadialForceComponent* RadialForceComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UProjectileMovementComponent* ProjectileComp;


	/* Type of damage dealt. */
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float DamageInnerRadius;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float DamageOuterRadius;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float DamageFalloff;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float BaseDamage;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float MinDamage;


	UPROPERTY(EditDefaultsOnly, Category = "Grenade")
	float ThrowSpeed;

	UPROPERTY(EditDefaultsOnly, Category = "Grenade")
	float FuseTime;


	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* ExplosionEffects;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	USoundBase* ExplosionSound;

public:	

	// Sets default values for this actor's properties
	ABoiGrenade();

	void PullFuse();

	void Throw(FVector Direction);

	UFUNCTION(BlueprintPure)
	UStaticMeshComponent* GetMesh() const { return MeshComp; }

protected:

	FTimerHandle TimerHandle_Fuse;

	void OnFuseEnded();

	UFUNCTION(NetMulticast, Reliable)
	void Client_PlayExplosionEffects();

	void PullFuse(float Time);
};
