// Copyright FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "BoiWeapon.generated.h"


class UCurveFloat;
class UParticleSystem;
class UParticleSystemComponent;
class USkeletalMeshComponent;
class UCameraShake;

class ABoiPlayerCharacter;
class UBoiWeaponDataAsset;


UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	Idle		UMETA(DisplayName = "Idle"),
	Firing		UMETA(DisplayName = "Firing"),
	Reloading	UMETA(DisplayName = "Reloading"),
	Equipping	UMETA(DisplayName = "Equipping")
};



USTRUCT()
struct FHitScanTrace
{
	GENERATED_BODY()

	UPROPERTY()
	TEnumAsByte<EPhysicalSurface> SurfaceType;

	UPROPERTY()
	FVector_NetQuantize TraceTo;
	
	UPROPERTY()
	FVector_NetQuantize ImpactNormal;

	UPROPERTY()
	AActor* HitActor;

	UPROPERTY()
	bool bHitActorWithHealth;

	UPROPERTY()
	bool bKilledActor;
};



UCLASS(HideCategories = ("Actor Tick", "Input"))
class FPS2018_API ABoiWeapon : public AActor
{
	GENERATED_BODY()

public:

	ABoiWeapon();

	virtual void PostInitializeComponents() override;

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, Category = "Weapon")
	EWeaponState GetCurrentState() const { return CurrentState; }

	bool IsAttachedToPawn() const;

	void SetOwningPawn(ABoiPlayerCharacter* NewOwner);

	USkeletalMeshComponent* GetWeaponMesh() const;

	UFUNCTION(BlueprintPure, Category = "Weapon")
	ABoiPlayerCharacter* GetOwningPawn() const { return MyPawn; }

	float GetADSFOV() const { return ADSFOV; }

	float GetADSSpeed() const { return ADSSpeed; }

protected:

	/* Muzzle Socket MUST have this name. */
	UPROPERTY(VisibleDefaultsOnly, Category = "Weapon")
	FName MuzzleSocketName = "MuzzleSocket";

	/* Shell eject socket MUST have this name. */
	UPROPERTY(VisibleDefaultsOnly, Category = "Weapon")
	FName ShellEjectSocketName = "ShellEjectSocket";

	/* Tracer end point MUST have this name. See TracerFX in Effects */
	UPROPERTY(VisibleDefaultsOnly, Category = "Weapon")
	FName TracerTargetName = "BeamEnd";
		
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* FPPMeshComp = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* TPPMeshComp = nullptr;

	ABoiPlayerCharacter* MyPawn;

	EWeaponState CurrentState = EWeaponState::Idle;

	void AttachMeshToPawn();

	void DetachMeshFromPawn();

	void DetermineWeaponState();

	void SetWeaponState(EWeaponState NewState);

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	UBoiWeaponDataAsset* WeaponData;


	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float ADSFOV = 65.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float ADSSpeed = 20.0f;

	/* RPM - Bullets per minute fired by weapon. */
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	float RateOfFire = 600.0f;



	/* Type of damage dealt. */
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	TSubclassOf<UDamageType> DamageType;

	/* Base amount of damage dealt on a hit. */
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float BaseDamage = 20.0f;

	/* Damage multiplier on vulnerable areas. */
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float HeadshotMultiplier = 3.0f;
	
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	UCurveFloat* DamageFalloffCurve = nullptr;
		


	/* Minimum amount of vertical kick on fire. */
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float MinVertRecoil = 0.1f;

	/* Maximum amount of vertical kick on fire. */
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float MaxVertRecoil = 0.5f;

	/* Minimum amount of horizontal kick on fire. */
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float MinHorzRecoil = -0.1f;

	/* Maximum amount of horizontal kick on fire. */
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float MaxHorzRecoil = 0.8f;

	UPROPERTY(EditDefaultsOnly, Category = "Accuracy", meta = (ClampMin = "0", UIMin = "0"))
	float ADSRecoilMultiplier = 0.9f;

	UPROPERTY(EditDefaultsOnly, Category = "Accuracy", meta = (ClampMin = "0", UIMin = "0"))
	float CrouchRecoilMultiplier = 0.9f;

	UPROPERTY(EditDefaultsOnly, Category = "Accuracy", meta = (ClampMin = "0", UIMin = "0"))
	float ProneRecoilMultiplier = 0.75f;

	UPROPERTY(EditDefaultsOnly, Category = "Accuracy", meta = (ClampMin = "0", UIMin = "0"))
	int32 RayScanRange = 10000.0f;



	/* Min bullet cone spread angle in degrees. */
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy", meta = (ClampMin = "0", ClampMax = "180", UIMin = "0", UIMax = "180"))
	float MinSpread = 0.5f;

	/* Max bullet cone spread angle in degrees. */
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy", meta = (ClampMin = "0", ClampMax = "180", UIMin = "0", UIMax = "180"))
	float MaxSpread = 3.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Accuracy", meta = (ClampMin = "0", UIMin = "0"))
	float SpreadDecreaseRate = 8.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Accuracy", meta = (ClampMin = "0", UIMin = "0"))
	float SpreadAngleIncreaseOnShotFired = 1.2f;



	/* Camera shake applied on fire and is visual only, doesn't affect accuracy/raycast */
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	TSubclassOf<UCameraShake> FireCamShake;


	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	uint32 bLoopedMuzzleFX : 1;

	/* Effect that plays at socket MuzzleSocketName */
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* MuzzleFX = nullptr;

	UParticleSystemComponent* MuzzlePSC = nullptr;
	UParticleSystemComponent* MuzzlePSCSecondary = nullptr;

	/* Effect that plays on raycast impact */
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* FleshImpactFX = nullptr;

	/* Effect that plays on raycast impact */
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* DefaultImpactFX = nullptr;

	/* Line effect for bullet trail */
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* TracerFX = nullptr;

	/* Bullet shell eject effects */
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* ShellFX = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UMaterialInterface* BulletHoleMaterial;


	/* Sound played when fire attempt is made when mag is empty */
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundBase* EmptyMagSound;

	/* Sound played when the weapon is fired. */
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundBase* FireSound;

public:

	
	bool CanFire() const;

	void StartFire();

	void StopFire();

	void ControlRecoil(float Pitch, float Yaw);
	
	UFUNCTION(BlueprintPure, Category = "Weapon")
	bool IsFiring() const { return bFiring; }
	
	UFUNCTION(BlueprintPure, Category = "Weapon")
	float GetCurrentSpread() const { return CurrentSpread; };

	UFUNCTION(BlueprintPure, Category = "Weapon")
	float GetMaxSpread() const { return MaxSpread; };

	UFUNCTION(BlueprintPure, Category = "Weapon")
	float GetMinSpread() const { return MinSpread; };

	UFUNCTION(BlueprintPure, Category = "Weapon")
	float GetVerticalRecoil() const { return MaxVertRecoil; };

	UFUNCTION(BlueprintPure, Category = "Weapon")
	float GetHorizontalRecoil() const { return MaxHorzRecoil; };

	UFUNCTION(BlueprintPure, Category = "Weapon")
	FString GetWeaponName() const;


protected:


	UPROPERTY(ReplicatedUsing = OnRep_HitScanTrace)
	FHitScanTrace HitScanTrace;


	virtual void Fire();

	void PlayFireEffects(FVector TraceEnd);

	void PlayImpactEffects(EPhysicalSurface SurfaceType, FVector ImpactPoint, FVector ImpactNormal);

	void ApplyRecoil();

	/* Apply changes to weapon spread based on stance and ADS */
	void WidenSpreadOnFire();

	void ShrinkSpread(float DeltaTime);

	void RecoilReturn(float DeltaTime);
		
	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayFireSound();
	
	FHitResult WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const;


private:

	FTimerHandle TimerHandle_TimeBetweenShots;

	float LastFiredTime = 0.0f;

	// Derived from RateOfFire
	float TimeBetweenShots = 0.0f;

	float CurrentSpread = 0.0f;
	
	bool bRecoilReturn;

	bool bFiring;
	
	FVector RecoilOffset;
	FVector RecoilControlOffset;
	
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire();

	UFUNCTION()
	void OnRep_HitScanTrace();

	

/************************************************************************/
/*                         EQUIP FUNCTIONALITY                          */
/************************************************************************/
protected:

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float EquipTime = .5f;

public:

	virtual void Equip(const ABoiWeapon* LastWeapon);
	
	virtual void Unequip();

	/** [server] weapon was added to pawn's inventory */
	virtual void OnEnterInventory(ABoiPlayerCharacter* NewOwner);

	/** [server] weapon was removed from pawn's inventory */
	virtual void OnLeaveInventory();

protected:

	virtual void OnEquipFinished();

	FTimerHandle TimerHandle_OnEquipFinished;

	bool bEquipped = false;

	bool bEquipping = false;

	float EquipStartTime = 0.0f;

/************************************************************************/
/*                          AMMO FUNCTIONALITY                          */
/************************************************************************/
protected:

	/* Number of rounds in gun before a reload is required. */
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	uint32 MaxAmmoInGun = 30;
	
	/* Number of rounds in backpack */
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	uint32 MaxAmmoInBackpack = 150;

public:
	
	UFUNCTION(BlueprintPure, Category = "Weapon")
	int GetCurrentAmmoInBackpack() const { return CurrentAmmoInBackpack; };

	UFUNCTION(BlueprintPure, Category = "Weapon")
	int GetCurrentAmmoInGun() const { return CurrentAmmoInWeapon; };

private:

	UPROPERTY(Transient, Replicated)
	uint32 CurrentAmmoInWeapon = 0;

	UPROPERTY(Transient, Replicated)
	uint32 CurrentAmmoInBackpack = 0;

/************************************************************************/
/*                         RELOAD FUNCTIONALITY                         */
/************************************************************************/
protected:

	/* Time in seconds needed to reload the weapon. */
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	float ReloadTime = 4.0f; 

public:
	UFUNCTION(BlueprintPure, Category = "Weapon")
	int GetReloadTime() const { return ReloadTime; }

	UFUNCTION(BlueprintPure, Category = "Weapon")
	bool IsReloading() const { return bReloading; };

	bool CanReload() const;
	
	virtual void StartReload(bool bFromReplication = false);

	virtual void StopReload();
	
protected:

	virtual void Reload();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerStartReload();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerStopReload();

	UFUNCTION()
	void OnRep_bReloading();

private:

	UPROPERTY(Transient, ReplicatedUsing = OnRep_bReloading)
	bool bReloading = false;
	
	FTimerHandle TimerHandle_Reload;
	FTimerHandle TimerHandle_StopReload;
};
