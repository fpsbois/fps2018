// Copyright © FPSBois 2018

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BoiWeaponDrop.generated.h"

class USphereComponent;

class ABoiWeapon;

UCLASS()
class ABoiWeaponDrop : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoiWeaponDrop();

	virtual void PostInitializeComponents() override;

	void SetWeapon(AActor* Instigator, ABoiWeapon* Weapon);

	ABoiWeapon* GetWeapon();

	ABoiWeapon* SwitchWeapon(ABoiWeapon* Weapon);

protected:

	UPROPERTY(EditDefaultsOnly, Category = "WeaponDrop")
	float m_LifeSpan = 30.0f;

	/* Weapon to spawn with */
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<ABoiWeapon> DefaultWeapon;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FHitResult m_Hit;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USphereComponent* SphereComp;

	UPROPERTY(Replicated)
	ABoiWeapon* m_Weapon;
	
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void DestroyWeaponDrop();

	FTimerHandle TimerHandle_EndLife;
};
